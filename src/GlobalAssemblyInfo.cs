﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("CleanZip")]
[assembly: AssemblyProduct("CleanZip")]
[assembly: AssemblyCopyright("Copyright © Jens Granlund 2012")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.1.1.0")]
[assembly: AssemblyFileVersion("1.1.1.0")]
[assembly: AssemblyInformationalVersionAttribute("1.1.1.0")]
