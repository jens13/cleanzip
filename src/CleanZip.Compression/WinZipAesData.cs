﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;
using System.Security.Cryptography;

namespace CleanZip.Compression
{
    internal class WinZipAesData
    {
        private const int Rfc2898Iterations = 1000;

        private byte[] _key;
        private byte[] _salt;
        private byte[] _initialVector;
        private byte[] _generatedPasswordVerification;
        private byte[] _storedPasswordVerification;
        private readonly int _keySizeInBytes;

        internal WinZipAesData(string password)
        {
            _keySizeInBytes = 32;
            
            GenerateSalt();
            
            GenerateKey(password);
        }

        internal WinZipAesData(string password, WinZipAesKeyStrength keyStrength, Stream stream)
        {
            _keySizeInBytes = keyStrength == WinZipAesKeyStrength.Aes128 ? 16 : 32;

            ReadSaltAndPasswordVerification(stream);

            GenerateKey(password);

            VerifyPassword();
        }

        internal void WriteSaltAndPasswordVerification(Stream stream)
        {
            stream.Write(_salt, 0, _salt.Length);
            stream.Write(_generatedPasswordVerification, 0, _generatedPasswordVerification.Length);
        }

        private void ReadSaltAndPasswordVerification(Stream stream)
        {
            _salt = new byte[_keySizeInBytes/2];
            stream.Read(_salt, 0, _keySizeInBytes/2);
            _storedPasswordVerification = new byte[2];
            stream.Read(_storedPasswordVerification, 0, 2);
        }

        private void GenerateKey(string password)
        {
            var rfc2898 = new Rfc2898DeriveBytes(password, _salt, Rfc2898Iterations);
            _key = rfc2898.GetBytes(_keySizeInBytes);
            _initialVector = rfc2898.GetBytes(_keySizeInBytes);
            _generatedPasswordVerification = rfc2898.GetBytes(2);
        }

        private void VerifyPassword()
        {
            var generated = BitConverter.ToInt16(_generatedPasswordVerification, 0);
            var stored = BitConverter.ToInt16(_storedPasswordVerification, 0);
            if (generated != stored) throw new WinZipAesException("Wrong password");
        }

        private void GenerateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            _salt = new byte[_keySizeInBytes / 2];
            rng.GetBytes(_salt);
        }

        internal byte[] Key { get { return _key; } }

        internal byte[] InitialVector { get { return _initialVector; } }
    }
}