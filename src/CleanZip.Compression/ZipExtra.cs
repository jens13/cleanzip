﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.Collections.Generic;

namespace CleanZip.Compression
{
    public class ZipExtra
    {
        private readonly Dictionary<Type, IZipExtraField> _extraFields = new Dictionary<Type, IZipExtraField>();

        private static readonly Dictionary<Type, ushort> ExtraFieldTemplates = new Dictionary<Type, ushort>
            {
                {typeof (WinZipAesExtra), WinZipAesExtra.HeaderId},
            };

        public static void AddExtraFieldType(Type extraFieldType, ushort headerId)
        {
            if (ExtraFieldTemplates.ContainsKey(extraFieldType)) return;
            ExtraFieldTemplates.Add(extraFieldType, headerId);
        }
        public void Parse(byte[] extra)
        {
            foreach (var extraFieldType in ExtraFieldTemplates.Keys)
            {
                var extraField = CreateExtraField(extraFieldType, ExtraFieldTemplates[extraFieldType], extra);
                if (extraField != null) WriteExtra(extraField);
            }
        }

        private IZipExtraField CreateExtraField(Type extraFieldType, ushort headerId, byte[] extra)
        {
            var field = FindExtraFieldHeaderId(headerId, extra);
            var pos = field.Item1;
            var len = field.Item2;
            if (pos == -1) return null;
            var buffer = new byte[len];
            Buffer.BlockCopy(extra, pos, buffer, 0, len);
            return (IZipExtraField)Activator.CreateInstance(extraFieldType, buffer);
        }

        private Tuple<int, int> FindExtraFieldHeaderId(ushort headerId, byte[] extra)
        {
            int pos = 0;
            if (extra.Length > 0)
            {
                do
                {
                    var headerIdFound = (ushort)(extra[pos] + extra[pos + 1] * 256);
                    var dataSize = (short)(extra[pos + 2] + extra[pos + 3] * 256);
                    if (headerId == headerIdFound) return new Tuple<int, int>(pos, dataSize + 4);
                    pos += dataSize + 4;
                } while (pos + 3 < extra.Length);
            }
            return new Tuple<int, int>(-1, 0);
        }

        public bool IsEmpty { get { return _extraFields.Count == 0; } }

        public bool IsDirty { get; private set; }

        public void SetExtra(IZipExtraField extraField)
        {
            WriteExtra(extraField);
            IsDirty = true;
        }

        internal void WriteExtra(IZipExtraField extraField)
        {
            var type = extraField.GetType();
            if (_extraFields.ContainsKey(type))
            {
                _extraFields[type] = extraField;
            }
            else
            {
                _extraFields.Add(type, extraField);
            }
        }

        public T GetField<T>() where T : IZipExtraField
        {
            var type = typeof (T);
            if (_extraFields.ContainsKey(type))
            {
                return (T)_extraFields[type];
            }
            return default(T);
        }

        public void ClearExtra<T>() where T : IZipExtraField
        {
            var type = typeof(T);
            if (_extraFields.ContainsKey(type))
            {
                _extraFields.Remove(type);
                IsDirty = true;
            }
        }

        public byte[] ToArray()
        {
            var array = new List<byte>();
            foreach(var extra in _extraFields.Values)
            {
                array.AddRange(extra.ToArray());
            }
            return array.ToArray();
        }
    }
}