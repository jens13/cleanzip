﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

namespace CleanZip.Compression
{
    public enum WinZipAesKeyStrength
    {
        Aes128 = 1,
        Aes256 = 3
    }
}