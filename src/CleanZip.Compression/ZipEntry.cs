﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace CleanZip.Compression
{
    public sealed class ZipEntry
    {
        private const int OptimalBufferSize = 4096;
        public const uint CentralHeaderSignature = 0x02014b50;
        public const uint LocalHeaderSignature = 0x04034b50;
        public const uint DataDescriptorSignature = 0x08074b50;

        private string _comment = "";
        private uint _compressedSize;
        private ushort _compressionMethod;
        private uint _crc32;

        private long _dataOffset;
        private ushort _diskNumberStart;
        private uint _externalAttributes;
        private ZipExtra _extra = new ZipExtra();
        private string _sourceFile;
        private Stream _sourceStream;
        private ushort _generalPurposeBitFlag;
        private ushort _internalAttributes;
        private DateTime _lastModified = DateTime.Now;

        private uint _localCompressedSize;
        private ushort _localCompressionMethod;
        private uint _localCrc32;
        private ushort _localGeneralPurposeBitFlag;
        private uint _localHeaderOffset;
        private bool _localHeaderRead;
        private DateTime _localLastModified = DateTime.MinValue;
        private string _localName = "";
        private uint _localUncompressedSize;
        private ushort _localVersionNeededToExtract;

        private string _name;
        private Stream _originalStream;
        private uint _uncompressedSize;
        private ushort _versionMadeBy = 10;
        private ushort _versionNeededToExtract = 10;
        private WinZipAesData _winZipAesData;
        private WinZipCryptoStream _winZipCryptoStream;
        private bool _isDirty;

        internal ZipEntry(string sourceFile, string entryName, CompressionType compressionType, string password)
            : this(sourceFile, entryName, compressionType)
        {
            SetEncryptionFlags(compressionType, password);
        }

        internal ZipEntry(string sourceFile, string entryName, CompressionType compressionType)
            : this(entryName, compressionType)
        {
            _sourceFile = sourceFile;
            var fi = new FileInfo(sourceFile);
            if (!fi.Exists) throw new FileNotFoundException("File not found", sourceFile);
            _lastModified = fi.LastWriteTime;
            _isDirty = true;
        }

        internal ZipEntry(Stream sourceStream, string entryName, CompressionType compressionType, string password)
            : this(sourceStream, entryName, compressionType)
        {
            SetEncryptionFlags(compressionType, password);
        }

        internal ZipEntry(Stream sourceStream, string entryName, CompressionType compressionType)
            : this(entryName, compressionType)
        {
            if (!sourceStream.CanRead) throw new IOException("Can not read from file stream");
            _sourceStream = sourceStream;
            _isDirty = true;
        }

        private ZipEntry(string entryName, CompressionType compressionType)
        {
            _name = NormalizeName(entryName);
            _compressionMethod = (ushort) compressionType;
            if (compressionType == CompressionType.Deflate)
            {
                _versionMadeBy = 20;
                _versionNeededToExtract = 20;
            }
            _isDirty = true;
        }

        internal ZipEntry(Stream stream)
        {
            _originalStream = stream;
            ReadCentralHeader();
        }

        public string Comment { get { return _comment; } set { _comment = value; } }

        public ZipExtra Extra { get { return _extra; } }

        public string Name { get { return _name; } }

        public uint LocalHeaderOffset { get { return _localHeaderOffset; } }

        public uint ExternalAttributes { get { return _externalAttributes; } }

        public ushort InternalAttributes { get { return _internalAttributes; } }

        public ushort DiskNumberStart { get { return _diskNumberStart; } }

        public uint UncompressedSize { get { return _uncompressedSize; } }

        public uint CompressedSize { get { return _compressedSize; } }

        public uint Crc32 { get { return _crc32; } }

        public DateTime LastModified { get { return _lastModified; } set { _lastModified = value; } }

        public CompressionType CompressionMethod
        {
            get
            {
                return _compressionMethod == 99
                           ? WinZipAesExtra.CompressionMethod
                           : (CompressionType) _compressionMethod;
            }
            private set
            {
                if (_compressionMethod == 99)
                {
                    WinZipAesExtra.CompressionMethod = value;
                }
                else
                {
                    _compressionMethod = (ushort) value;
                }
            }
        }

        public ushort GeneralPurposeBitFlag { get { return _generalPurposeBitFlag; } }

        public ushort VersionNeededToExtract { get { return _versionNeededToExtract; } }

        public ushort VersionMadeBy { get { return _versionMadeBy; } }

        public bool UseUtf8Encoding
        {
            get { return (GeneralPurposeBitFlag & GeneralPurposeType.UseUtf8Encoding) == GeneralPurposeType.UseUtf8Encoding; }
            set
            {
                if (value)
                {
                    _generalPurposeBitFlag = (ushort)(_generalPurposeBitFlag | GeneralPurposeType.UseUtf8Encoding);
                }
                else
                {
                    _generalPurposeBitFlag = (ushort)(_generalPurposeBitFlag & ~GeneralPurposeType.UseUtf8Encoding);
                }
            }
        }

        public bool UseDataDescriptor
        {
            get
            {
                return (GeneralPurposeBitFlag & GeneralPurposeType.UseDataDescriptor) ==
                       GeneralPurposeType.UseDataDescriptor;
            }
            set
            {
                if (value)
                {
                    _generalPurposeBitFlag = (ushort) (_generalPurposeBitFlag | GeneralPurposeType.UseDataDescriptor);
                }
                else
                {
                    _generalPurposeBitFlag = (ushort) (_generalPurposeBitFlag & ~GeneralPurposeType.UseDataDescriptor);
                }
            }
        }

        public Encoding Encoding
        {
            get
            {
                if (UseUtf8Encoding) return Encoding.UTF8;
                return Encoding.GetEncoding(437);
            }
        }

        public bool UseDataDescriptorSignature { get; set; }

        public bool IsDirty
        {
            get { return _isDirty || _extra.IsDirty; }
        }

        public WinZipAesExtra LocalWinZipAesExtra { get { return _extra.GetField<WinZipAesExtra>(); } set { _extra.SetExtra(value); } }

        public WinZipAesExtra WinZipAesExtra { get { return _extra.GetField<WinZipAesExtra>(); } set { _extra.SetExtra(value); } }

        private void SetEncryptionFlags(CompressionType compressionType, string password)
        {
            _winZipAesData = new WinZipAesData(password);
            _extra.SetExtra(new WinZipAesExtra(compressionType));
            _compressionMethod = 99;
            _generalPurposeBitFlag = (ushort)(_generalPurposeBitFlag | GeneralPurposeType.WinZipAes);
        }

        private void ClearEncryptionFlags()
        {
            _generalPurposeBitFlag = (ushort)(_generalPurposeBitFlag & ~GeneralPurposeType.WinZipAes);
            _compressionMethod = (ushort)WinZipAesExtra.CompressionMethod;
            _extra.ClearExtra<WinZipAesExtra>();
            _winZipAesData = null;
        }

        internal void SetNewFile(string sourceFile, CompressionType compressionType, string password)
        {
            SetEncryptionFlags(compressionType, password);
            SetNewFile(sourceFile, compressionType);
        }
        internal void SetNewFile(string sourceFile, CompressionType compressionType)
        {
            var fi = new FileInfo(sourceFile);
            if (!fi.Exists) throw new FileNotFoundException("File not found", sourceFile);
            if (_sourceStream != null)
            {
                _sourceStream.Close();
                _sourceStream = null;
            }
            _sourceFile = sourceFile;
            _lastModified = fi.LastWriteTime;
            _originalStream = null;
            _isDirty = true;
        }

        internal void SetNewStream(Stream sourceStream, CompressionType compressionType, string password)
        {
            SetEncryptionFlags(compressionType, password);
            SetNewStream(sourceStream, compressionType);
        }
        internal void SetNewStream(Stream sourceStream, CompressionType compressionType)
        {
            if (!sourceStream.CanRead) throw new IOException("Can not read from file stream");
            if (_sourceStream != null)
            {
                _sourceStream.Close();
                _sourceStream = null;
            }
            _sourceStream = sourceStream;
            _originalStream = null;
            _isDirty = true;
        }

        public void ClearExtraField()
        {
            _extra = new ZipExtra();
        }

        public static string NormalizeName(string entryName)
        {
            string name = entryName.Replace('\\', '/');

            int position = name.IndexOf(':');
            if (position > -1) name = name.Substring(position);

            return name.Trim('/');
        }

        private void ReadCentralHeader()
        {
            #region Central Header byte map
            // central file header signature   4 bytes  (0x02014b50)
            // version made by                 2 bytes
            // version needed to extract       2 bytes
            // general purpose bit flag        2 bytes
            // compression method              2 bytes
            // last mod file time              2 bytes
            // last mod file date              2 bytes
            // crc-32                          4 bytes
            // compressed size                 4 bytes
            // uncompressed size               4 bytes
            // file name length                2 bytes
            // extra field length              2 bytes
            // file comment length             2 bytes
            // disk number start               2 bytes
            // internal file attributes        2 bytes
            // external file attributes        4 bytes
            // relative offset of local header 4 bytes

            //file name (variable size)
            //extra field (variable size)
            //file comment (variable size)
            #endregion

            _versionMadeBy = _originalStream.ReadUShort();
            _versionNeededToExtract = _originalStream.ReadUShort();
            _generalPurposeBitFlag = _originalStream.ReadUShort();
            _compressionMethod = _originalStream.ReadUShort();
            _lastModified = _originalStream.ReadDateTime();
            _crc32 = _originalStream.ReadUInt();
            _compressedSize = _originalStream.ReadUInt();
            _uncompressedSize = _originalStream.ReadUInt();
            ushort fileNameLength = _originalStream.ReadUShort();
            ushort extraFieldLength = _originalStream.ReadUShort();
            ushort fileCommentLength = _originalStream.ReadUShort();
            _diskNumberStart = _originalStream.ReadUShort();
            _internalAttributes = _originalStream.ReadUShort();
            _externalAttributes = _originalStream.ReadUInt();
            _localHeaderOffset = _originalStream.ReadUInt();

            _name = (fileNameLength == 0) ? "" : _originalStream.ReadString(fileNameLength, Encoding);
            if (extraFieldLength > 0)
            {
                var extraField = new byte[extraFieldLength];
                _originalStream.Read(extraField, 0, extraFieldLength);
                _extra.Parse(extraField);
            }
            _comment = (fileCommentLength == 0) ? "" : _originalStream.ReadString(fileCommentLength, Encoding);
        }

        public void ValidateLocalHeader()
        {
            if (!_localHeaderRead) ReadLocalHeader();
            if (_localVersionNeededToExtract != VersionNeededToExtract)
                throw new ZipException(string.Format("VersionNeededToExtract local header missmatch for entry {0}", Name));
            if (_localGeneralPurposeBitFlag != GeneralPurposeBitFlag)
                throw new ZipException(string.Format("GeneralPurposeBitFlag local header missmatch for entry {0}", Name));
            if (_localCompressionMethod != _compressionMethod)
                throw new ZipException(string.Format("CompressionMethod local header missmatch for entry {0}", Name));
            if (_localLastModified != LastModified)
                throw new ZipException(string.Format("LastModified local header missmatch for entry {0}", Name));
            if (_localCrc32 != Crc32)
                throw new ZipException(string.Format("Crc32 local header missmatch for entry {0}", Name));
            if (_localCompressedSize != CompressedSize)
                throw new ZipException(string.Format("CompressedSize local header missmatch for entry {0}", Name));
            if (_localUncompressedSize != UncompressedSize)
                throw new ZipException(string.Format("UncompressedSize local header missmatch for entry {0}", Name));
            if (_localName != Name)
                throw new ZipException(string.Format("Name local header missmatch for entry {0}", Name));
        }

        private void ReadLocalHeader()
        {
            #region Local Header byte map
            // local file header signature     4 bytes  (0x04034b50)
            // version needed to extract       2 bytes
            // general purpose bit flag        2 bytes
            // compression method              2 bytes
            // last mod file time              2 bytes
            // last mod file date              2 bytes
            // crc-32                          4 bytes
            // compressed size                 4 bytes
            // uncompressed size               4 bytes
            // file name length                2 bytes
            // extra field length              2 bytes

            // file name (variable size)
            // extra field (variable size)
            #endregion

            if (_localHeaderRead) return;
            long streamPos = _originalStream.Position;
            _originalStream.Seek(LocalHeaderOffset + 4, SeekOrigin.Begin);

            Encoding encoding = Encoding.GetEncoding(437);
            _localVersionNeededToExtract = _originalStream.ReadUShort();
            _localGeneralPurposeBitFlag = _originalStream.ReadUShort();
            _localCompressionMethod = _originalStream.ReadUShort();
            _localLastModified = _originalStream.ReadDateTime();
            _localCrc32 = _originalStream.ReadUInt();
            _localCompressedSize = _originalStream.ReadUInt();
            _localUncompressedSize = _originalStream.ReadUInt();
            ushort fileNameLength = _originalStream.ReadUShort();
            ushort extraFieldLength = _originalStream.ReadUShort();

            if ((_localGeneralPurposeBitFlag & 0x0800) == 0x0800) encoding = Encoding.UTF8;
            _localName = (fileNameLength == 0) ? "" : _originalStream.ReadString(fileNameLength, encoding);
            if (extraFieldLength > 0)
            {
                var extraField = new byte[extraFieldLength];
                _originalStream.Read(extraField, 0, extraFieldLength);
                _extra.Parse(extraField);
            }
            _dataOffset = _originalStream.Position;

            if (UseDataDescriptor)
            {
                _originalStream.Seek(CompressedSize, SeekOrigin.Current);
                uint crc = _originalStream.ReadUInt();
                if (crc == DataDescriptorSignature)
                {
                    UseDataDescriptorSignature = true;
                    crc = _originalStream.ReadUInt();
                }
                _localCrc32 = crc;
                _localCompressedSize = _originalStream.ReadUInt();
                _localUncompressedSize = _originalStream.ReadUInt();
            }
            _originalStream.Position = streamPos;
            _localHeaderRead = true;
        }

        public void WriteCentralRecord(Stream stream)
        {
            stream.WriteUInt(CentralHeaderSignature);
            stream.WriteUShort(VersionMadeBy);
            stream.WriteUShort(VersionNeededToExtract);
            stream.WriteUShort(GeneralPurposeBitFlag);
            stream.WriteUShort(_compressionMethod);
            stream.WriteDateTime(LastModified);
            stream.WriteUInt(Crc32);
            stream.WriteUInt(CompressedSize);
            stream.WriteUInt(UncompressedSize);
            byte[] fileName = Encoding.GetBytes(Name);
            stream.WriteUShort((ushort) fileName.Length);
            byte[] extraField = _extra.ToArray();
            stream.WriteUShort((ushort) extraField.Length);
            byte[] fileComment = Encoding.GetBytes(Comment);
            stream.WriteUShort((ushort) fileComment.Length);
            stream.WriteUShort(DiskNumberStart);
            stream.WriteUShort(InternalAttributes);
            stream.WriteUInt(ExternalAttributes);
            stream.WriteUInt(LocalHeaderOffset);
            stream.Write(fileName, 0, fileName.Length);
            stream.Write(extraField, 0, extraField.Length);
            stream.Write(fileComment, 0, fileComment.Length);
            stream.Flush();
        }

        public void WriteEntry(Stream stream)
        {
            if (_originalStream != null) ReadLocalHeader();
            _localHeaderOffset = (uint) stream.Position;
            stream.WriteUInt(LocalHeaderSignature);
            stream.WriteUShort(VersionNeededToExtract);
            stream.WriteUShort(GeneralPurposeBitFlag);
            stream.WriteUShort(_compressionMethod);
            stream.WriteDateTime(LastModified);
            stream.WriteUInt(Crc32);
            stream.WriteUInt(CompressedSize);
            stream.WriteUInt(UncompressedSize);
            byte[] fileName = Encoding.GetBytes(Name);
            stream.WriteUShort((ushort) fileName.Length);
            byte[] extraField = _extra.ToArray();
            stream.WriteUShort((ushort)extraField.Length);
            stream.Write(fileName, 0, fileName.Length);
            stream.Write(extraField, 0, extraField.Length);

            var dataOffset = stream.Position;
            WriteData(stream);
            stream.Flush();

            bool compressionMethodChanged = false;
            if (UncompressedSize == 0)
            {
                CompressionMethod = (ushort)CompressionType.Store;
            }
            else if (CompressedSize > UncompressedSize)
            {
                stream.Position = dataOffset;
                if (_sourceStream != null) _sourceStream.Position = 0;
                CompressionMethod = (ushort) CompressionType.Store;
                WriteData(stream);
                stream.Flush();
                compressionMethodChanged = true;
            }

            if (UseDataDescriptor)
            {
                if (UseDataDescriptorSignature)
                {
                    stream.WriteUInt(DataDescriptorSignature);
                }
                stream.WriteUInt(Crc32);
                stream.WriteUInt(CompressedSize);
                stream.WriteUInt(UncompressedSize);
                stream.Flush();
            }

            long streamPos = stream.Position;
            stream.Position = LocalHeaderOffset + 8;
            stream.WriteUShort(_compressionMethod);
            stream.WriteDateTime(LastModified);
            stream.WriteUInt(Crc32);
            stream.WriteUInt(CompressedSize);
            stream.WriteUInt(UncompressedSize);
            if (compressionMethodChanged)
            {
                stream.WriteUShort((ushort)fileName.Length);
                extraField = _extra.ToArray();
                stream.WriteUShort((ushort) extraField.Length);
                stream.Write(fileName, 0, fileName.Length);
                stream.Write(extraField, 0, extraField.Length);
            }
            stream.Flush();
            stream.Position = streamPos;
            _isDirty = false;
        }

        private void WriteData(Stream stream)
        {
            if (_originalStream != null)
            {
                WriteOriginalData(stream);
                return;
            }
            var closeSourceStreamAfterWrite = OpenFileIfSourceStreamIsNull();
            var startPosition = stream.Position;
            var targetStream = CreateWriteEntryStream(stream);

            WriteSaltAndPasswordVerificationIfEncrypted(stream);
            if (WinZipAesExtra != null && WinZipAesExtra.Vendor == WinZipAesType.AE2)
            {
                WriteEntryToTargetStream(targetStream);
            }
            else
            {
                WriteEntryToTargetStreamWithCrc32(targetStream);
            }


            if (_compressionMethod != 0)
            {
                targetStream.Close();
            }

            WriteAuthenticationCodeIfEncrypted(stream);

            _compressedSize = (uint)(stream.Position - startPosition);
            if (closeSourceStreamAfterWrite)
            {
                _sourceStream.Close();
                _sourceStream = null;
            }
        }

        private Stream CreateWriteEntryStream(Stream targetStream)
        {
            var stream = targetStream;
            var isEncrypting = _compressionMethod == 99;
            if (isEncrypting)
            {
                if (WinZipAesExtra == null || _winZipAesData == null) throw new WinZipAesException("Encryption information is missing in archive");
                stream = _winZipCryptoStream = WinZipCryptoStream.Create(stream, _winZipAesData);
            }
            if (CompressionMethod == CompressionType.Deflate)
            {
#if DOTNET45
                stream = new DeflateStream(stream, CompressionLevel.Optimal, !isEncrypting);
#else
                stream = new DeflateStream(stream, CompressionMode.Compress, !isEncrypting);
#endif
            }
            return stream;
        }

        private void WriteSaltAndPasswordVerificationIfEncrypted(Stream stream)
        {
            if (_compressionMethod != 99) return;
            if (_winZipCryptoStream == null) return;
            _winZipAesData.WriteSaltAndPasswordVerification(stream);
        }

        private void WriteAuthenticationCodeIfEncrypted(Stream stream)
        {
            if (_compressionMethod != 99) return;
            if (_winZipCryptoStream == null) return;
            stream.Write(_winZipCryptoStream.AuthenticationCode, 0, 10);
        }

        private void WriteEntryToTargetStreamWithCrc32(Stream targetStream)
        {
            _uncompressedSize = 0;
            _crc32 = Crc32Hash.DefaultSeed;
            int bytesRead;
            var buffer = new byte[OptimalBufferSize];
            var originalPosition = _sourceStream.Position;
            try
            {
                _sourceStream.Position = 0;
                do
                {
                    bytesRead = _sourceStream.Read(buffer, 0, OptimalBufferSize);
                    if (bytesRead <= 0) break;
                    _uncompressedSize += (uint) bytesRead;
                    targetStream.Write(buffer, 0, bytesRead);
                    _crc32 = Crc32Hash.CalculateHash(_crc32, buffer, bytesRead);
                } while (bytesRead == OptimalBufferSize);
            }
            finally
            {
                _sourceStream.Position = originalPosition;
            }
            _crc32 ^= Crc32Hash.DefaultSeed;
            targetStream.Flush();
        }
        private void WriteEntryToTargetStream(Stream targetStream)
        {
            _uncompressedSize = 0;
            int bytesRead;
            var buffer = new byte[OptimalBufferSize];
            var originalPosition = _sourceStream.Position;
            try
            {
                _sourceStream.Position = 0;
                do
                {
                    bytesRead = _sourceStream.Read(buffer, 0, OptimalBufferSize);
                    if (bytesRead <= 0) break;
                    _uncompressedSize += (uint) bytesRead;
                    targetStream.Write(buffer, 0, bytesRead);
                } while (bytesRead == OptimalBufferSize);
            }
            finally
            {
                _sourceStream.Position = originalPosition;
            }
            _crc32 = 0;
            targetStream.Flush();
        }

        private bool OpenFileIfSourceStreamIsNull()
        {
            if (_sourceStream == null)
            {
                _sourceStream = File.OpenRead(_sourceFile);
                return true;
            }
            return false;
        }

        private void WriteOriginalData(Stream outStream)
        {
            long streamPos = _originalStream.Position;
            _originalStream.Position = _dataOffset;
            var buffer = new byte[OptimalBufferSize];
            long size = CompressedSize;
            while (size > 0)
            {
                var length = (int) Math.Min(size, buffer.Length);
                int bytesRead = _originalStream.Read(buffer, 0, length);
                if (bytesRead <= 0) break;
                size -= bytesRead;
                outStream.Write(buffer, 0, bytesRead);
            }
            outStream.Flush();
            _originalStream.Position = streamPos;
        }
        internal void Extract(Stream stream)
        {
            Extract(stream, null);
        }
        internal void Extract(Stream targetStream, string password)
        {
            if (_originalStream == null) throw new ZipException(string.Format("{0} entry has not been written to archive.", Name));

            ReadLocalHeader();
            if (_localUncompressedSize == 0) return;
            
            var streamPosition = _originalStream.Position;
            _originalStream.Position = _dataOffset;
            
            var entryStream = CreateReadEntryStream(password);
            ExtractToTargetStream(entryStream, targetStream);

            VerifyAuthenticationCodeIfEncrypted();

            if (_compressionMethod != 0)
            {
                entryStream.Close();
            }
            _originalStream.Position = streamPosition;
            targetStream.Flush();
        }

        private void VerifyAuthenticationCodeIfEncrypted()
        {
            if (_compressionMethod != 99) return;
            if (_winZipCryptoStream == null) return;
            var buffer = new byte[10];
            _originalStream.Read(buffer, 0, 10);
            if (buffer.ArrayEquals(_winZipCryptoStream.AuthenticationCode)) return;
            throw new WinZipAesException("Encrypton Authentication Code error");
        }

        private void ExtractToTargetStream(Stream entryStream, Stream targetStream)
        {
            var buffer = new byte[OptimalBufferSize];
            long size = _localUncompressedSize;
            while (size > 0)
            {
                var length = (int) Math.Min(size, buffer.Length);
                var bytesRead = entryStream.Read(buffer, 0, length);
                if (bytesRead <= 0) break;
                size -= bytesRead;
                targetStream.Write(buffer, 0, bytesRead);
            }
            targetStream.Flush();
        }

        private Stream CreateReadEntryStream(string password)
        {
            var stream = _originalStream;
            var isEncrypted = _compressionMethod == 99;
            if (isEncrypted)
            {
                var winZipAesExtra = WinZipAesExtra;
                if (winZipAesExtra == null) throw new WinZipAesException("Encryption information is missing in archive");
                var compressedSize = _localCompressedSize - 12 - (winZipAesExtra.KeyStrength == WinZipAesKeyStrength.Aes128 ? 8 : 16);
                _winZipAesData = new WinZipAesData(password, winZipAesExtra.KeyStrength, stream);
                stream = _winZipCryptoStream = WinZipCryptoStream.OpenRead(stream, _winZipAesData, compressedSize);
            }
            if (CompressionMethod == CompressionType.Deflate)
            {
                stream = new DeflateStream(stream, CompressionMode.Decompress, !isEncrypted);
            }
            return stream;
        }

        public Stream GetStream()
        {
            return GetStream(null);
        }
        public Stream GetStream(string password)
        {
            var memoryStream = new MemoryStream();
            Extract(memoryStream, password);
            memoryStream.Position = 0;
            return memoryStream;
        }

        internal void DisconnectStream()
        {
            _sourceStream = null;
        }
    }
}