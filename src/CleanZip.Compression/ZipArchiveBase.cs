﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CleanZip.Compression
{
    public abstract class ZipArchiveBase
    {
        public const uint CentralEndRecordSignature = 0x06054b50;

        protected Stream ArchiveStream;
        protected readonly List<ZipEntry> EntryList = new List<ZipEntry>();

        protected bool IsNew;

        protected uint CentralDirectoryOffset;
        private uint _centralDirectorySize;
        private ushort _totalEntriesInCentralDir;
        private ushort _totalEntriesInCentralDirOnThisDisk;

        private byte[] _fileComment = new byte[0];

        private ushort _numberOfThisDisk;
        private ushort _numberOfThisDiskFromCentralDir;

        private int _useDataDescriptor = -1;
        private int _useDataDescriptorSignature = -1;
        private int _useUtf8Encoding = -1;
        private bool _allowReplacingEntries;
        private bool _isChanged;
        private bool _isDirty;

        protected bool IsChanged
        {
            get { return _isChanged || EntryList.Any(e => e.IsDirty); }
            set { _isChanged = value; }
        }
        protected bool IsDirty
        {
            get { return _isDirty || EntryList.Any(e => e.IsDirty); }
            set { _isDirty = value; }
        }

        public bool AllowReplacingEntries
        {
            get { return _allowReplacingEntries; }
            set
            {
                if (_allowReplacingEntries == value) return;
                if (IsNew && EntryList.Count > 0)
                    throw new ZipException("Allow overwriting can not be changed after entries has been added");
                _allowReplacingEntries = value;
            }
        }

        public bool UseDataDescriptorSignature
        {
            get
            {
                if (_useDataDescriptorSignature == -1 && EntryList.Count > 0)
                {
                    return EntryList[0].UseDataDescriptorSignature;
                }
                return _useDataDescriptorSignature == 1;
            }
            set { _useDataDescriptorSignature = value ? 1 : 0; }
        }

        public bool UseUtf8Encoding
        {
            get
            {
                if (_useUtf8Encoding == -1)
                {
                    if (EntryList.Count > 0)
                    {
                        return EntryList[0].UseUtf8Encoding;
                    }
                    return true;
                }
                return _useUtf8Encoding == 1;
            }
            set { _useUtf8Encoding = value ? 1 : 0; }
        }

        public bool UseDataDescriptor
        {
            get
            {
                if (_useDataDescriptor == -1 && EntryList.Count > 0)
                {
                    return EntryList[0].UseDataDescriptor;
                }
                return _useDataDescriptor == 1;
            }
            set { _useDataDescriptor = value ? 1 : 0; }
        }

        public string FileComment
        {
            get
            {
                var encoding = UseUtf8Encoding ? Encoding.UTF8 : Encoding.ASCII;
                return encoding.GetString(_fileComment);
            }
            set
            {
                var encoding = UseUtf8Encoding ? Encoding.UTF8 : Encoding.ASCII;
                _fileComment = encoding.GetBytes(value);
            }
        }

        public ZipEntry Find(string entryName)
        {
            string name = ZipEntry.NormalizeName(entryName);
            return EntryList.FirstOrDefault(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        protected void ReadCentralEndRecord()
        {
            #region Central End Record byte map

            // end of central dir signature    4 bytes  (0x06054b50)
            // number of this disk             2 bytes
            // number of the disk with the
            // start of the central directory  2 bytes
            // total number of entries in the
            // central directory on this disk  2 bytes
            // total number of entries in
            // the central directory           2 bytes
            // size of the central directory   4 bytes
            // offset of start of central
            // directory with respect to
            // the starting disk number        4 bytes
            // .ZIP file comment length        2 bytes
            // .ZIP file comment       (variable size)

            #endregion

            _numberOfThisDisk = ArchiveStream.ReadUShort();
            _numberOfThisDiskFromCentralDir = ArchiveStream.ReadUShort();
            _totalEntriesInCentralDirOnThisDisk = ArchiveStream.ReadUShort();
            _totalEntriesInCentralDir = ArchiveStream.ReadUShort();
            _centralDirectorySize = ArchiveStream.ReadUInt();
            CentralDirectoryOffset = ArchiveStream.ReadUInt();
            ushort fileCommentLength = ArchiveStream.ReadUShort();

            _fileComment = new byte[fileCommentLength];
            if (fileCommentLength > 0) ArchiveStream.Read(_fileComment, 0, fileCommentLength);
        }

        protected void ReadCentralDirectory()
        {
            ArchiveStream.Seek(CentralDirectoryOffset, SeekOrigin.Begin);
            while (ArchiveStream.ReadUInt() == ZipEntry.CentralHeaderSignature)
            {
                EntryList.Add(new ZipEntry(ArchiveStream));
            }
        }

        protected void WriteCentralDirectory(Stream stream)
        {
            CentralDirectoryOffset = (uint)stream.Position;
            foreach (ZipEntry entry in EntryList)
            {
                entry.WriteCentralRecord(stream);
            }
            _centralDirectorySize = (uint)(stream.Position - CentralDirectoryOffset);
            _totalEntriesInCentralDir = (ushort)EntryList.Count;
            _totalEntriesInCentralDirOnThisDisk = _totalEntriesInCentralDir;
        }

        protected void WriteCentralEndRecord(Stream stream)
        {
            stream.WriteUInt(CentralEndRecordSignature);
            stream.WriteUShort(_numberOfThisDisk);
            stream.WriteUShort(_numberOfThisDiskFromCentralDir);
            stream.WriteUShort(_totalEntriesInCentralDirOnThisDisk);
            stream.WriteUShort(_totalEntriesInCentralDir);
            stream.WriteUInt(_centralDirectorySize);
            stream.WriteUInt(CentralDirectoryOffset);
            stream.WriteUShort((ushort)_fileComment.Length);
            stream.Write(_fileComment, 0, _fileComment.Length);
            stream.Flush();
            stream.SetLength(stream.Position);
        }

        protected void RemoveCentralDirectoryAndEndRecord(Stream stream)
        {
            if (CentralDirectoryOffset == 0) return;
            stream.Position = CentralDirectoryOffset;
            CentralDirectoryOffset = 0;
        }


        protected ZipEntry ChangeOrCreateZipEntry(string entryName, Action<ZipEntry> setMethod, Func<string, ZipEntry> createMethod)
        {
            string name = ZipEntry.NormalizeName(entryName);
            ZipEntry zipEntry = Find(name);
            if (zipEntry != null)
            {
                if (!_allowReplacingEntries || setMethod == null) throw new ZipException(string.Format("Entry {0} already exists.", entryName));
                setMethod(zipEntry);
                IsChanged = true;
            }
            else
            {
                zipEntry = createMethod(name);
                zipEntry.UseUtf8Encoding = UseUtf8Encoding;
                zipEntry.UseDataDescriptor = UseDataDescriptor;
                zipEntry.UseDataDescriptorSignature = UseDataDescriptorSignature;
                EntryList.Add(zipEntry);
                if (IsNew && !_allowReplacingEntries) zipEntry.WriteEntry(ArchiveStream);
            }
            IsDirty = true;
            return zipEntry;
        }

        public byte[] StreamToArray()
        {
            long position = 0;
            if (ArchiveStream.CanSeek)
            {
                position = ArchiveStream.Position;
                ArchiveStream.Position = 0;
            }
            try
            {
                var memoryStream = ArchiveStream as MemoryStream;
                return memoryStream != null ? memoryStream.ToArray() : StreamToArray(ArchiveStream);
            }
            finally
            {
                if (ArchiveStream.CanSeek)
                {
                    ArchiveStream.Position = position;
                }
            }
        }

        private byte[] StreamToArray(Stream stream)
        {
            var buffer = new byte[16*1024];
            using (var memoryStream = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, read);
                }
                return memoryStream.ToArray();
            }        
        }

        protected void CloseStreamOrLeaveOpen(bool leaveOpen)
        {
            if (leaveOpen)
            {
                ArchiveStream.Flush();
                ArchiveStream.Position = 0;
            }
            else
            {
                ArchiveStream.Close();
                ArchiveStream = null;
            }
        }
    }
}