﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;

namespace CleanZip.Compression
{
    public sealed class ZipOutputStream : ZipArchiveBase, IDisposable
    {

        private bool _isClosed;
        private readonly bool _leaveOpen;

        private ZipOutputStream(Stream archiveStream, bool leaveOpen)
        {
            if (archiveStream == null) throw new ZipException("Archive stream can not be null");
            if (!archiveStream.CanWrite) throw new ZipException("Archive stream can not be written to");
            if (!archiveStream.CanSeek) throw new ZipException("Archive stream can not be written to");
            archiveStream.Position = 0;

            ArchiveStream = archiveStream;
            _leaveOpen = leaveOpen;
            IsNew = true;
            AllowReplacingEntries = false;
        }

        public static ZipOutputStream Create()
        {
            return new ZipOutputStream(new MemoryStream(), false);
        }

        public static ZipOutputStream Wrap(Stream archiveStream, bool leaveOpen)
        {
            return new ZipOutputStream(archiveStream, leaveOpen);
        }


        public void Add(string fileName, CompressionType compressionType)
        {
            string entryName = Path.GetFileName(fileName);
            Add(fileName, entryName, compressionType);
        }

        public void Add(string fileName, string entryName, CompressionType compressionType)
        {
            RemoveCentralDirectoryAndEndRecord(ArchiveStream);
            Func<string, ZipEntry> createMethod = name => new ZipEntry(fileName, name, compressionType);
            ChangeOrCreateZipEntry(entryName, null, createMethod);
        }

        public void Add(string fileName, string entryName, CompressionType compressionType, string password)
        {
            RemoveCentralDirectoryAndEndRecord(ArchiveStream);
            Func<string, ZipEntry> createMethod = name => new ZipEntry(fileName, name, compressionType, password);
            ChangeOrCreateZipEntry(entryName, null, createMethod);
        }
        
        public void Add(Stream fileStream, string entryName, CompressionType compressionType)
        {
            RemoveCentralDirectoryAndEndRecord(ArchiveStream);
            Func<string, ZipEntry> createMethod = name => new ZipEntry(fileStream, name, compressionType);
            ChangeOrCreateZipEntryAndDisconnectStream(entryName, createMethod);
        }

        public void Add(Stream fileStream, string entryName, CompressionType compressionType, string password)
        {
            RemoveCentralDirectoryAndEndRecord(ArchiveStream);
            Func<string, ZipEntry> createMethod = name => new ZipEntry(fileStream, name, compressionType, password);
            ChangeOrCreateZipEntryAndDisconnectStream(entryName, createMethod);
        }

        private void ChangeOrCreateZipEntryAndDisconnectStream(string entryName, Func<string, ZipEntry> createMethod)
        {
            var zipEntry = ChangeOrCreateZipEntry(entryName, null, createMethod);
            zipEntry.DisconnectStream();
        }

        public byte[] ToArray()
        {
            if (_isClosed) throw new ObjectDisposedException("ZipOutputStream");
            Flush();
            return StreamToArray();
        }

        public void Dispose()
        {
            Close();
        }

        public void Close()
        {
            if (_isClosed) return;
            _isClosed = true;
            Flush();
            if (!_leaveOpen)
            {
                ArchiveStream.Close();
            }
            ArchiveStream = null;
        }

        private void Flush()
        {
            if (CentralDirectoryOffset != 0) return;
            WriteCentralDirectory(ArchiveStream);
            WriteCentralEndRecord(ArchiveStream);
            IsDirty = false;
        }
    }
}