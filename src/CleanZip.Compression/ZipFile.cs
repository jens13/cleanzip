//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;
using System.Threading;

namespace CleanZip.Compression
{
    public sealed class ZipFile : ZipArchive
    {
        private readonly string _archiveFile;

        private ZipFile(string file, bool isReadOnly) :base(isReadOnly)
        {
            _archiveFile = file;
            var fileInfo = new FileInfo(file);
            if (fileInfo.Exists && fileInfo.Length < 22) throw new IOException(string.Format("File '{0}' is to small to be a zip archive", Path.GetFileName(file)));
            IsNew = !fileInfo.Exists;
            if (isReadOnly)
            {
                ArchiveStream = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            else
            {
                ArchiveStream = File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            }
            AllowReplacingEntries = !isReadOnly;
            if (IsNew) return;
            FindCentralEndRecord();
            ReadCentralDirectory();
        }

        /// <summary>
        /// Opens or creates a Zip file 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static ZipFile Open(string file)
        {
            return new ZipFile(file, false);
        }
        /// <summary>
        /// Opens a Zip file read only
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static ZipFile OpenRead(string file)
        {
            if (!File.Exists(file)) throw new FileNotFoundException(string.Format("File '{0}' not found", Path.GetFileName(file)), file);
            return new ZipFile(file, true);
        }

        public string FileName { get { return _archiveFile; } }

        public static bool Contains(string archive, string entryName)
        {
            using (var zf = new ZipFile(archive, true))
            {
                return (zf.Find(ZipEntry.NormalizeName(entryName)) != null);
            }
        }

        public static void ExtractAll(string archive, string folder, bool overwrite)
        {
            using (var zf = new ZipFile(archive, true))
            {
                zf.ExtractAll(folder, overwrite);
            }
        }

        public void WriteToFile(string file)
        {
            using (var stream = File.Open(file, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
            {
                WriteToStream(stream);
            }
        }

        public override void Close()
        {
            if (ArchiveStream != null)
            {
                if (IsDirty)
                {
                    InternalFlush(false);
                }
                else
                {
                    CloseStreamOrLeaveOpen(false);
                }
            }
        }

        protected override void InternalFlush(bool leaveOpen)
        {
            if (!SimpleFlush(leaveOpen))
            {
                string tempFile = Path.Combine(Path.GetDirectoryName(_archiveFile) ?? Path.GetTempPath(),
                                                Guid.NewGuid() + ".tmpzipfile");
                WriteToFile(tempFile);
                CloseMergeFiles();
                ArchiveStream.Close();
                ArchiveStream = null;
                try
                {
                    DeleteFile(_archiveFile);
                }
                catch (IOException)
                {
                    DeleteFile(tempFile);
                    throw new ZipException("Could not update zip file");
                }
                File.Move(tempFile, _archiveFile);
                if (leaveOpen)
                {
                    ArchiveStream = File.Open(_archiveFile, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
            }
            IsNew = false;
            IsChanged = false;
            IsDirty = false;
        }

        public static void DeleteFile(string file)
        {
            if (file == null) throw new ArgumentNullException("file");
            int attempts = 0;
            while (File.Exists(file) && attempts < 10)
            {
                try
                {
                    attempts++;
                    if (File.Exists(file)) File.Delete(file);
                }
                catch (IOException)
                {
                    Thread.Sleep(0);
                }
            }
        }
    }
}