//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

namespace CleanZip.Compression
{
    public struct GeneralPurposeType
    {
        public const ushort UseDataDescriptor = 0x0008;
        public const ushort UseUtf8Encoding = 0x0800;
        public const ushort WinZipAes = 0x0001; 
   }
}