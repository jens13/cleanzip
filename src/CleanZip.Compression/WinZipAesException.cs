﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;

namespace CleanZip.Compression
{
    public class WinZipAesException : Exception
    {
        public WinZipAesException()
            : base("Error reading Aes excrypted zip file")
        {
        }

        public WinZipAesException(string message) : base(message)
        {
        }
    }
}