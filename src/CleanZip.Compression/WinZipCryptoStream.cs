﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;
using System.Security.Cryptography;

namespace CleanZip.Compression
{
    internal class WinZipCryptoStream : Stream
    {
        private const int BlockSizeInBytes = 16;
        private const int BlockSizeInBits = 128;
        private const int KeySizeInBits = 256;

        private readonly Stream _internalStream;
        private readonly WinZipAesData _winZipAesData;
        private readonly CryptoStreamMode _cryptoStreamMode;

        private long _totalBytesProcessed;
        private bool _lastBlockIsProcessed;

        private readonly byte[] _inputCryptoBuffer = new byte[BlockSizeInBytes];
        private byte[] _outputCryptoBuffer = new byte[BlockSizeInBytes];
        private int _nonce = 1;

        private readonly HMACSHA1 _hmacsha1;

        private readonly AesManaged _aesManaged;
        private readonly ICryptoTransform _cryptoTransform;
        private int _pendingCount;
        private readonly byte[] _pendingBuffer;

        private WinZipCryptoStream(Stream stream, WinZipAesData winZipAesData, CryptoStreamMode cryptoStreamMode)
        {
            _internalStream = stream;
            _winZipAesData = winZipAesData;
            _cryptoStreamMode = cryptoStreamMode;

            int keySizeInBits = KeySizeInBits;
            if (_cryptoStreamMode == CryptoStreamMode.Read)
            {
                keySizeInBits = _winZipAesData.Key.Length * 8;
                if (keySizeInBits != 256 && keySizeInBits != 128 && keySizeInBits != 192)
                {
                    throw new WinZipAesException("Keysize have to be 128, 192, or 256");
                }
            }
            else
            {
                _pendingBuffer = new byte[BlockSizeInBytes];
            }
            var initialVector = new byte[BlockSizeInBytes];
            _hmacsha1 = new HMACSHA1(_winZipAesData.InitialVector, true);
            _aesManaged = new AesManaged { BlockSize = BlockSizeInBits, KeySize = keySizeInBits, Mode = CipherMode.ECB, Padding = PaddingMode.None };
            _cryptoTransform = _aesManaged.CreateEncryptor(_winZipAesData.Key, initialVector);
        }

        internal static WinZipCryptoStream OpenRead(Stream stream, WinZipAesData winZipAesData, long compressedSize)
        {
            var winZipCryptoStream = new WinZipCryptoStream(stream, winZipAesData, CryptoStreamMode.Read);
            winZipCryptoStream.StreamLength = compressedSize;
            return winZipCryptoStream;
        }


        public static WinZipCryptoStream Create(Stream stream, WinZipAesData winZipAesData)
        {
            return new WinZipCryptoStream(stream, winZipAesData, CryptoStreamMode.Write);
        }

        private long StreamLength { get; set; }

        public override void Flush()
        {
            _internalStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (!CanRead) throw new NotSupportedException();
            ValidateArguments(buffer, offset, count);

            if (_totalBytesProcessed >= StreamLength) return 0;

            var bytesRemaining = (int)(StreamLength - _totalBytesProcessed);
            var bytesToRead = Math.Min(bytesRemaining, count);
            var bytesRead = _internalStream.Read(buffer, offset, bytesToRead);

            var localCount = bytesRead;
            var localOffset = offset;
            while(localCount > BlockSizeInBytes)
            {
                DecryptBlockInPlace(buffer, localOffset, BlockSizeInBytes);
                localCount -= BlockSizeInBytes;
                localOffset += BlockSizeInBytes;
            }
            DecryptBlockInPlace(buffer, localOffset, localCount);

            return bytesRead;
        }

        private void DecryptBlockInPlace(byte[] buffer, int offset, int count)
        {
            IncreaseNonceAndCopyToInputCryptoBuffer();

            if (_totalBytesProcessed + count == StreamLength)
            {
                _hmacsha1.TransformFinalBlock(buffer, offset, count);
                _outputCryptoBuffer = _cryptoTransform.TransformFinalBlock(_inputCryptoBuffer, 0, BlockSizeInBytes);
                _lastBlockIsProcessed = true;
            }
            else
            {
                _hmacsha1.TransformBlock(buffer, offset, count, null, 0);
                _cryptoTransform.TransformBlock(_inputCryptoBuffer, 0, BlockSizeInBytes, _outputCryptoBuffer, 0);
            }
            XorBufferWithCipherStream(buffer, offset, count);
            _totalBytesProcessed += count;
        }

        private void XorBufferWithCipherStream(byte[] buffer, int offset, int count)
        {
            for (var i = 0; i < count; i++)
            {
                buffer[offset + i] = (byte)(_outputCryptoBuffer[i] ^ buffer[offset + i]);
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (_lastBlockIsProcessed) throw new InvalidOperationException("The authentication block has already been transformed.");
            if (!CanWrite) throw new NotSupportedException();
            ValidateArguments(buffer, offset, count);

            if (count + _pendingCount < BlockSizeInBytes)
            {
                Buffer.BlockCopy(buffer, offset, _pendingBuffer, _pendingCount, count);
                _pendingCount += count;
                return;
            }
            int localOffset = offset;
            int localCount = count;
            if (_pendingCount > 0)
            {
                var pendingFillCount = BlockSizeInBytes - _pendingCount;
                Buffer.BlockCopy(buffer, offset, _pendingBuffer, _pendingCount, pendingFillCount);
                EncryptPendingBlockAndWriteToStream();
                localOffset += pendingFillCount;
                localCount -= pendingFillCount;
            }
            _pendingCount = BlockSizeInBytes;
            while (localCount >= BlockSizeInBytes)
            {
                Buffer.BlockCopy(buffer, localOffset, _pendingBuffer, 0, BlockSizeInBytes);
                EncryptPendingBlockAndWriteToStream();
                localCount -= BlockSizeInBytes;
                localOffset += BlockSizeInBytes;
            }
            if (localCount > 0)
            {
                Buffer.BlockCopy(buffer, localOffset, _pendingBuffer, 0, localCount);
            }
            _pendingCount = localCount;
        }

        private void EncryptPendingBlockAndWriteToStream()
        {
            IncreaseNonceAndCopyToInputCryptoBuffer();
            _cryptoTransform.TransformBlock(_inputCryptoBuffer, 0, BlockSizeInBytes, _outputCryptoBuffer, 0);
            XorBufferWithCipherStream(_pendingBuffer, 0, BlockSizeInBytes);
            _hmacsha1.TransformBlock(_pendingBuffer, 0, BlockSizeInBytes, null, 0);
            _internalStream.Write(_pendingBuffer, 0, BlockSizeInBytes);
        }

        private void IncreaseNonceAndCopyToInputCryptoBuffer()
        {
            Buffer.BlockCopy(BitConverter.GetBytes(_nonce++), 0, _inputCryptoBuffer, 0, 4);
        }

        private void EncryptLastPendingBlockAndWriteToStream()
        {
            IncreaseNonceAndCopyToInputCryptoBuffer();
            _outputCryptoBuffer = _cryptoTransform.TransformFinalBlock(_inputCryptoBuffer, 0, BlockSizeInBytes);
            XorBufferWithCipherStream(_pendingBuffer, 0, _pendingCount);
            _hmacsha1.TransformFinalBlock(_pendingBuffer, 0, _pendingCount);
            _internalStream.Write(_pendingBuffer, 0, _pendingCount);
            _lastBlockIsProcessed = true;
        }

        public byte[] AuthenticationCode
        {
            get
            {
                if (!_lastBlockIsProcessed)
                {
                    if (_totalBytesProcessed != 0)
                    {
                        throw new InvalidOperationException("The final hash has not been computed.");
                    }

                    // Computet empty hash for zero file length
                    _hmacsha1.ComputeHash(new byte[] {});
                }
                var authenticationCode = new byte[10];
                Array.Copy(_hmacsha1.Hash, 0, authenticationCode, 0, 10);
                return authenticationCode;
            }
        }

        private static void ValidateArguments(byte[] buffer, int offset, int count)
        {
            if (buffer == null) throw new ArgumentNullException("buffer");
            if (offset < 0) throw new ArgumentOutOfRangeException("offset", "Have to be greater than zero.");
            if (count < 0) throw new ArgumentOutOfRangeException("count", "Have to be greater than zero.");
            if (buffer.Length < offset + count) throw new ArgumentException("The buffer is too small");
        }

        public override void Close()
        {
            if (_pendingCount > 0)
            {
                EncryptLastPendingBlockAndWriteToStream();
            }
            if (!_lastBlockIsProcessed)
            {
                _hmacsha1.TransformFinalBlock(_pendingBuffer, 0, _pendingCount);
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override bool CanRead { get { return _cryptoStreamMode == CryptoStreamMode.Read; } }

        public override bool CanSeek { get { return false; } }

        public override bool CanWrite { get { return _cryptoStreamMode == CryptoStreamMode.Write; } }

        public override long Length { get { return StreamLength; } }

        public override long Position { get { throw new NotSupportedException(); } set { throw new NotSupportedException(); } }

    }
}