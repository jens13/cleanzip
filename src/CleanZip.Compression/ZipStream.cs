﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

using System;
using System.IO;

namespace CleanZip.Compression
{
    public sealed class ZipStream : ZipArchive
    {
        private readonly bool _leaveOpen;
        private bool _isClosed;

        private ZipStream(Stream archiveStream, bool isReadOnly, bool leaveOpen)
            : base(isReadOnly)
        {
            if (archiveStream == null) throw new ZipException("Archive stream can not be null");
            if (!archiveStream.CanSeek) throw new ZipException("Archive stream can not be read from or written to");
            if (!archiveStream.CanWrite && !isReadOnly) throw new ZipException("Archive stream can not be written to");
            if (!archiveStream.CanRead && isReadOnly) throw new ZipException("Archive stream can not be read from");
            if (isReadOnly && archiveStream.Length > 0 && archiveStream.Length < 22) throw new IOException(string.Format("Stream is to small to be a zip archive"));

            archiveStream.Position = 0;
            ArchiveStream = archiveStream;
            _leaveOpen = leaveOpen;

            IsNew = archiveStream.Length == 0;
            AllowReplacingEntries = !isReadOnly;
            if (IsNew) return;
            FindCentralEndRecord();
            ReadCentralDirectory();
        }

        public static ZipStream Wrap(Stream archiveStream, bool leaveOpen)
        {
            bool canWrite = archiveStream.CanWrite;
            return new ZipStream(archiveStream, !canWrite, leaveOpen);
        }

        public static ZipStream WrapRead(Stream archiveStream, bool leaveOpen)
        {
            return new ZipStream(archiveStream, true, leaveOpen);
        }

        public override void Close()
        {
            if (_isClosed) return;
            _isClosed = true;
            InternalFlush(_leaveOpen);
            if (!_leaveOpen)
            {
                ArchiveStream.Close();
            }
            ArchiveStream = null;
        }

        public byte[] ToArray()
        {
            if (_isClosed) throw new ObjectDisposedException("ZipOutputStream");
            InternalFlush(!_leaveOpen);
            return StreamToArray();
        }

        protected override void InternalFlush(bool leaveOpen)
        {
            if (!SimpleFlush(leaveOpen))
            {
                var tempStream = new MemoryStream();
                WriteToStream(tempStream);
                CloseMergeFiles();
                ArchiveStream.Close();
                ArchiveStream = leaveOpen ? null : tempStream;
            }
            IsNew = false;
            IsChanged = false;
            IsDirty = false;
        }
    }
}