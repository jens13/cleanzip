﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)
// Source: https://bitbucket.org/jens13/cleanzip

namespace CleanZip.Compression
{
    public enum WinZipAesType
    {
        AE1 = 0x0001,
        AE2 = 0x0002
    }
}