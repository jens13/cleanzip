﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using CleanZip.Compression;

namespace CleanZip
{
    public sealed class ZipArchive : IArchive
    {
        private readonly ZipFile _zipFile;

        public ZipArchive(ZipFile zipFile)
        {
            _zipFile = zipFile;
        }

        public void Dispose()
        {
            _zipFile.Dispose();
        }

        public void Add(IFile file)
        {
            _zipFile.Add(file.FileName, file.EntryName, CompressionType.Deflate);
        }

        public void Close()
        {
            _zipFile.Close();
        }
    }
}