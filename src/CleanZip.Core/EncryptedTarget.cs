﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using CleanZip.Compression;

namespace CleanZip
{
    public class EncryptedTarget : ITarget
    {
        private readonly string _fileName;
        private readonly string _password;

        public EncryptedTarget(string fileName, string password)
        {
            _fileName = fileName;
            _password = password;
        }

        public IArchive CreateArchive()
        {
            return new EncryptedArchive(ZipFile.Open(_fileName), _password);
        }
    }
}