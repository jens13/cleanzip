﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using CleanZip.Compression;

namespace CleanZip
{
    public sealed class EncryptedArchive : IArchive
    {
        private readonly ZipFile _zipFile;
        private readonly string _password;

        public EncryptedArchive(ZipFile zipFile, string password)
        {
            _zipFile = zipFile;
            _password = password;
        }

        public void Dispose()
        {
            _zipFile.Dispose();
        }

        public void Add(IFile file)
        {
            _zipFile.Add(file.FileName, file.EntryName, CompressionType.Deflate, _password);
        }

        public void Close()
        {
            _zipFile.Close();
        }
    }
}