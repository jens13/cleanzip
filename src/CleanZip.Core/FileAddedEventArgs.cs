﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;

namespace CleanZip
{
    public class FileAddedEventArgs : EventArgs
    {
        public FileAddedEventArgs(IFile file)
        {
            File = file;
        }

        public IFile File { get; private set; }
    }
}