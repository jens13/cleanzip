﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using CleanZip.Compression;

namespace CleanZip
{
    public class ZipTarget : ITarget
    {
        private readonly string _fileName;

        public ZipTarget(string fileName)
        {
            _fileName = fileName;
        }

        public IArchive CreateArchive()
        {
            return new ZipArchive(ZipFile.Open(_fileName));
        }
    }
}