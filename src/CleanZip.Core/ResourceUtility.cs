﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.IO;
using System.Linq;

namespace CleanZip
{
    public class ResourceUtility
    {
        public static Stream LoadResource(string name)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var streamName =
                    assembly.GetManifestResourceNames().FirstOrDefault(
                        n => n.EndsWith(name, StringComparison.OrdinalIgnoreCase));
                if (streamName == null) continue;
                return assembly.GetManifestResourceStream(streamName);
            }

            return null;
        }
        public static void ExctractIfNotExists(string name, string targetFile)
        {
            if (File.Exists(targetFile)) return;
            var directoryName = Path.GetDirectoryName(targetFile);
            if (!string.IsNullOrWhiteSpace(directoryName)) Directory.CreateDirectory(directoryName);
            using (var resource = LoadResource(name))
            using (var output = File.Create(targetFile))
            {
                resource.CopyTo(output);
            }
        }

    }
}