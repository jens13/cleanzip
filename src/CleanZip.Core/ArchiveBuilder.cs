﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;

namespace CleanZip
{
    public class ArchiveBuilder
    {
        private readonly ITarget _target;

        public ArchiveBuilder(ITarget target)
        {
            _target = target;
        }

        public event EventHandler<FileAddedEventArgs> FileAdded;

        public void OnFileAdded(FileAddedEventArgs e)
        {
            var handler = FileAdded;
            if (handler == null) return;
            handler(this, e);
        }

        public void BuildArchive(ISource source, IFilter filter)
        {
            var files = source.ApplyFilter(filter);
            using (var archive = _target.CreateArchive())
            {
                foreach (var file in files)
                {
                    archive.Add(file);
                    OnFileAdded(new FileAddedEventArgs(file));
                }
            }
        }
    }
}