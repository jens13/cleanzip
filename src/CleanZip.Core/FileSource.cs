﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip
{
    public class FileSource : IFile
    {
        public FileSource(string fileName, string entryName)
        {
            EntryName = entryName;
            FileName = fileName;
        }

        public string FileName { get; private set; }
        public string EntryName { get; private set; }
    }
}