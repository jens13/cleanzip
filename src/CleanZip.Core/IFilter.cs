﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip
{
    public interface IFilter
    {
        bool Inclusive(IFile file);
    }
}