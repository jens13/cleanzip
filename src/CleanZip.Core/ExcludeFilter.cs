﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.Linq;

namespace CleanZip
{
    public class ExcludeFilter :IFilter
    {
        private readonly bool _caseSensitive;
        private readonly List<List<string>> _folderPatterns;
        private readonly List<List<string>> _filePatterns;

        public ExcludeFilter(string excludePattern, bool caseSensitive)
        {
            _caseSensitive = caseSensitive;
            _filePatterns = new List<List<string>>();
            _folderPatterns = new List<List<string>>();

            var exclude = _caseSensitive ? excludePattern : excludePattern.ToUpperInvariant();
            var patterns = exclude.Replace('/', '\\').Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            
            foreach (var pattern in patterns)
            {
                var parts = pattern.Split(new[] {'*'}, StringSplitOptions.RemoveEmptyEntries).ToList();
                parts = parts.Select(p => p.Trim('?')).ToList();
                var trimedPattern = pattern.TrimEnd();
                if (trimedPattern.EndsWith("\\"))
                {
                    _folderPatterns.Add(parts);
                }
                else
                {
                    if (trimedPattern.EndsWith("*"))
                    {
                        parts.Add("*");
                    }
                    _filePatterns.Add(parts);
                }
            }
        }
        public bool Inclusive(IFile file)
        {
            var fileName = _caseSensitive ? file.FileName:file.FileName.ToUpperInvariant();
            if (FilePatternFound(fileName)) return false;
            if (FolderPatternFound(fileName)) return false;
            return true;
        }

        private bool FolderPatternFound(string fileName)
        {
            return _folderPatterns.Any(folderPattern => IndexOf(fileName, folderPattern) >= 0);
        }

        private bool FilePatternFound(string fileName)
        {
            return _filePatterns.Any(pattern => EndsWith(fileName, pattern));
        }

        private static int IndexOf(string text, IReadOnlyList<string> pattern)
        {
            var pos = 0;
            var textLength = text.Length;
            var patternIndex = 0;
            var patternLength = pattern.Count;
            var startPos = 0;
            while (pos < textLength)
            {
                var part = pattern[patternIndex];
                var partEnd = part.Length - 1;
                while (pos < textLength && part[0] != text[pos]) pos++;
                if (pos >= textLength) return -1;
                startPos = startPos == 0?pos:startPos;
                var partPos = 0;
                while (partPos <= partEnd)
                {
                    if (pos >= textLength) return -1;
                    if (part[partPos] != '?' && part[partPos] != text[pos])
                    {
                        startPos = 0;
                        break;
                    }
                    pos++;
                    partPos++;
                }
                if (partPos > partEnd) patternIndex++;
                if (patternIndex >= patternLength) return startPos;
            }
            return -1;
        }

        private static bool EndsWith(string text, IReadOnlyList<string> pattern)
        {
            var pos = text.Length - 1;
            var patternIndex = pattern.Count - 1;
            while (pos >= 0)
            {
                int partPos;
                string part;
                if (pattern[patternIndex] != "*")
                {
                    part = pattern[patternIndex];
                    for (partPos = part.Length - 1; partPos >= 0; partPos--)
                    {
                        if (pos < 0) return false;
                        if (part[partPos] != '?' && part[partPos] != text[pos]) return false;
                        pos--;
                    }
                }
                if (pos < 0) return false;
                patternIndex--;
                if (patternIndex < 0) return true;
                part = pattern[patternIndex];
                partPos = part.Length - 1;
                while (pos >= 0 && part[partPos] != text[pos]) pos--;
            }
            return false;
        }
    }
}