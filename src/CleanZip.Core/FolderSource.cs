﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CleanZip
{
    public class FolderSource : ISource
    {
        private readonly string _path;
        private readonly int _pathLength;

        public FolderSource(string path)
        {
            _path = path;
            _pathLength = _path.Length;
        }

        public IEnumerable<IFile> ApplyFilter(IFilter filter)
        {
            return Directory.EnumerateFiles(_path, "*.*", SearchOption.AllDirectories).Select(CreateFile).Where(filter.Inclusive);
        }

        private IFile CreateFile(string fileName)
        {
            var entryName = fileName.Substring(_pathLength);
            return new FileSource(fileName, entryName);
        }
    }
}