﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Diagnostics;
using System.IO;
using CommandLine;

namespace CleanZip.CmdConsole
{
    static class Program
    {
        public static string ExcludeTxtFullPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CleanZip", ExcludeTxtResourceName);
        public const string ExcludeTxtResourceName = "exclude.txt";

        [STAThread]
        static int Main(string [] args)
        {
            var returnCode = 0;
            try
            {
                ExtractExcludeTxtIfNotExists();
                var arguments = ParseCommandLineArguments(args);
                if (arguments == null) return 1;

                if (arguments.Preferences)
                {
                    Process.Start(ExcludeTxtFullPath);
                    return 0;
                }

                var folderSource = CreateSource(arguments);
                var excludeFilter = CreateFilter(arguments);
                var archiveBuilder = CreateBuilder(arguments);

                if (arguments.Verbose)
                {
                    archiveBuilder.FileAdded +=
                        (sender, eventArgs) => Console.WriteLine("Added: {0}", eventArgs.File.FileName);
                }
                archiveBuilder.BuildArchive(folderSource, excludeFilter);
            }
            catch(Exception ex)
            {
                Console.WriteLine("---- Error ----");
                Console.WriteLine(ex.ToString());
                returnCode = -1;
            }
            return returnCode;
        }

        private static void ExtractExcludeTxtIfNotExists()
        {
            ResourceUtility.ExctractIfNotExists(ExcludeTxtResourceName, ExcludeTxtFullPath);
        }

        private static CommandLineOptions ParseCommandLineArguments(string[] args)
        {
            var arguments = new CommandLineOptions();
            var parser = new CommandLineParser(new CommandLineParserSettings(true, true, false, Console.Error));
            if (!parser.ParseArguments(args, arguments)) return null;
            return arguments;
        }

        private static ArchiveBuilder CreateBuilder(CommandLineOptions args)
        {
            var zipTarget = args.Password == null
                                    ? (ITarget) new ZipTarget(args.ZipFile)
                                    : new EncryptedTarget(args.ZipFile, args.Password);
            return new ArchiveBuilder(zipTarget);
        }

        private static IFilter CreateFilter(CommandLineOptions args)
        {
            var excludeFilter = args.ExcludeFilter;
            if (string.IsNullOrWhiteSpace(excludeFilter)) return new IncludeAllFilter();
            return new ExcludeFilter(excludeFilter, args.CaseSensitiv);
        }

        private static ISource CreateSource(CommandLineOptions args)
        {
            return new FolderSource(args.SourceFolder);
        }
    }
}
