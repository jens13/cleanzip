﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.IO;
using CommandLine;
using CommandLine.Text;

namespace CleanZip.CmdConsole
{
    internal sealed class CommandLineOptions : CommandLineOptionsBase
    {
        [Option("s", "source", Required = false, HelpText = "Folder to add to zip file, if omitted current folder will be used.")]
        public string Source { get; set; }

        [Option("o", "output", Required = false, HelpText = "Output zip file, if omitted source folder name + todays date will be used.")]
        public string Output { get; set; }

        [Option("f", "filter", Required = false, HelpText = "A string with wildcard patterns separated by space, files with a match in it's path will be excluded")]
        public string Filter { get; set; }

        [Option("e", "exclude", Required = false, DefaultValue = false, HelpText = "Does not use filter from exclude.txt.")]
        public bool Exclude { get; set; }

        [Option("x", "excludetxt", Required = false, DefaultValue = false, HelpText = "Opens exclude.txt in default texteditor.")]
        public bool Preferences { get; set; }

        [Option("c", "casesensitiv", Required = false, DefaultValue = false, HelpText = "Makes filters case sensivie.")]
        public bool CaseSensitiv { get; set; }

        [Option("p", "password", Required = false, HelpText = "If supplied archive will be encrypted with AES-256.")]
        public string Password { get; set; }

        [Option("v", "verbose", Required = false, HelpText = "Prints additional information.")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        public string SourceFolder { get { return Source ?? Directory.GetCurrentDirectory(); } }

        public string ZipFile { get { return Output ?? CreateNewZipFile(); } }

        public string ExcludeFilter { get { return CreateExcludeFilter(); } }

        private string CreateNewZipFile()
        {
            int count = 1;
            var newZipFile = string.Format("{0}_{1}.zip", SourceFolder, DateTime.Now.ToString("yyyy-MM-dd"));
            while (File.Exists(newZipFile))
            {
                newZipFile = string.Format("{0}_{1}-{2}.zip", SourceFolder, DateTime.Now.ToString("yyyy-MM-dd"), count);
                count++;
            }
            return newZipFile;
        }

        private string CreateExcludeFilter()
        {
            var excludeFile = Program.ExcludeTxtFullPath;
            var filter = Filter ?? "";
            if (File.Exists(excludeFile) && !Exclude)
            {
                filter += " " + File.ReadAllText(excludeFile).Replace('\r', ' ').Replace('\n', ' ');
            }
            return filter;
        }
    }
}