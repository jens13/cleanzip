﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.IO;

namespace CleanZip.Test.Mock1
{
    public class TxtFilter : IFilter
    {
        public bool Inclusive(IFile file)
        {
            return (Path.GetExtension(file.FileName)??"").ToUpperInvariant() == ".TXT";
        }
    }
}