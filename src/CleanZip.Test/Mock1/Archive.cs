﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip.Test.Mock1
{
    public class Archive : IArchive
    {
        public void Dispose()
        {
            Close();
        }

        public bool ArchiveClosed { get; private set; }

        public int FilesCount { get; private set; }

        public void Add(IFile file)
        {
            FilesCount++;
        }

        public void Close()
        {
            ArchiveClosed = true;
        }
    }
}