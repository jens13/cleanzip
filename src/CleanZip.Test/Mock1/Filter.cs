﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip.Test.Mock1
{
    public class Filter : IFilter
    {
        public bool Inclusive(IFile file)
        {
            return true;
        }
    }
}