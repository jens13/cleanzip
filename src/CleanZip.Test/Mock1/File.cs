﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip.Test.Mock1
{
    public class File : IFile
    {
        public string FileName { get { return "test"; } }
        public string EntryName { get { return "test"; } }
    }
}