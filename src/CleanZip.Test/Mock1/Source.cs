﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CleanZip.Test.Mock1
{
    public class Source : ISource
    {
        private readonly List<IFile> _files;

        public Source()
        {
            _files = new List<IFile>();
            _files.Add(new File());
            _files.Add(new File());
        }
        public Source(IEnumerable<string> files)
        {
            _files = new List<IFile>();
            _files.AddRange(files.Select(CreateFile));
        }

        private IFile CreateFile(string fileName)
        {
            var entryName = Path.GetFileName(fileName);
            return new FileSource(fileName, entryName);
        }

        public IEnumerable<IFile> ApplyFilter(IFilter filter)
        {
            return _files.Where(filter.Inclusive);
        }
    }
}