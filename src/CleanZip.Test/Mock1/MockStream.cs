﻿using System;
using System.IO;

namespace CleanZip.Test.Mock1
{
    public class MockStream : Stream
    {
        private bool _canWrite;
        private bool _canSeek;

        public MockStream(bool canWrite, bool canSeek)
        {
            _canWrite = canWrite;
            _canSeek = canSeek;
        }

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek { get { return _canSeek; } }

        public override bool CanWrite { get { return _canWrite; } }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}