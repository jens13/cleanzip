﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

namespace CleanZip.Test.Mock1
{
    public class Target : ITarget
    {
        private Archive _archive;
        public int AddedFiles { get { return _archive.FilesCount; } }
        public bool ArchiveCreated { get { return _archive != null; } }
        public bool ArchiveClosed { get { return _archive.ArchiveClosed; } }

        public IArchive CreateArchive()
        {
            _archive = new Archive();
            return _archive;
        }
    }
}