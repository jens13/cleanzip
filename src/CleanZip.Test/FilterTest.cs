﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace CleanZip.Test
{
    public class FilterTest
    {
        private const int AdditionalFiles = 50;
        private const string AdditionalFilter = @" Thumbs.db *.~* *.7z *.aps *.bak *.bpl *.dcp *.dcu *.ddp *.dsm *.dsk *.gpState *.ilk *.lib *.log *.ncb *.obj *.ocxdebug *.pch *.pdb *.pidb *.resharper* *.resources *.sbr *.scc *.StyleCop *.suo *.tlb *.tlh *.used *.user *.userprefs *.vspscc *.vsssccc *.webinfo *.zip";

        [Fact]
        public void FilterRegex1()
        {
            const string filter = @"debug/ Debug/ release/ Release/ bin/ obj/ .Lib *.bak *.user _ReSharper*/ " + AdditionalFilter;
            var excludeFilter = new ExcludeFilterRegex(filter);

            RunTest(excludeFilter, 3 + 3 * AdditionalFiles);
        }
        [Fact]
        public void FilterRegex2()
        {
            const string filter = @"[Dd]ebug*/ [Rr]elease*/ [Bb]in\ [Oo]bj\ .bak *.user _ReSharper*/ " + AdditionalFilter + " *-?Copy.* *-?Copy?(*).*";
            var excludeFilter = new ExcludeFilterRegex(filter);

            RunTest(excludeFilter, 3);
        }

        [Fact]
        public void Filter1()
        {
            const string filter = @"debug/ Debug/ release/ Release/ bin/ obj/ .Lib *.bak *.user _ReSharper*/" + AdditionalFilter;
            var excludeFilter = new ExcludeFilter(filter, true);

            RunTest(excludeFilter, 3 + 3*AdditionalFiles);
        }
        [Fact]
        public void Filter2()
        {
            const string filter = @"Debug*/ Release*/ bin\ obj\ .bak *.lib *.user _ReSharper*/ " + AdditionalFilter + " *-?Copy.* *-?Copy?(*).*";
            var excludeFilter = new ExcludeFilter(filter,false);

            RunTest(excludeFilter, 3);
        }

        private static void RunTest(IFilter excludeFilter, int numberOfFiles)
        {
            var files = CreateFilesList();
            var source = new Mock1.Source(files);
            var timer = Stopwatch.StartNew();
            var filteredFiles = source.ApplyFilter(excludeFilter).ToList();
            timer.Stop();
            Console.WriteLine("Elapsed time: {0}", timer.ElapsedMilliseconds);
            filteredFiles.ForEach(ff => Console.WriteLine(ff.FileName));

            Assert.Equal(numberOfFiles, filteredFiles.Count);
        }

        private static IEnumerable<string> CreateFilesList()
        {
            var files = new List<string>();
            files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\debug\test1.csproj");
            files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.bak");
            files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.csproj");
            files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.cs");
            files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagad.Library\CleanZip\test1.cs");
            for (int i = 0; i < AdditionalFiles; i++)
            {
                // Folder filter
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\bin\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\obj\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\Debug\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\debug\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\Release\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\release\test1.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\_ReSharper.CleanZip\AspFileDataCache.dat");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\_ReSharper.CleanZip\test\AspFileDataCache.dat");
                // File filter
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.bak");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.zip");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.7z");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.lib");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1.csproj.user");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1 - Copy.csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1 - Copy (2).csproj");
                files.Add(@"c:\test\dadratratesljönlkeshiojneknlkaijfmdnsafsdf\dsagadgea\CleanZip\test\test1 - Copy (21).csproj");
            }
            return files;
        }
    }
}
