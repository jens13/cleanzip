﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System.Collections.Generic;
using System.IO;
using System.Text;
using CleanZip.Compression;
using Xunit;

namespace CleanZip.Test
{
    public class AddRemoveEntryTest : ZipTestBase
    {
        private string[] _filenames = new[]
            {
                "AAA",
                "BBB",
                "CCC",
                "EEE",
                "DDD",
            };

        [Fact]
        public void FileAddEntryTest()
        {
            var testZip = "testAdd.zip";
            var content = GenerateContent(100);
            var buffer = Encoding.ASCII.GetBytes(content);
            using (var zf = ZipFile.Open(testZip))
            {
                foreach (var filename in _filenames)
                {
                    zf.Add(new MemoryStream(buffer), filename, CompressionType.Deflate);
                }
            }

            using (var zf = ZipFile.Open(testZip))
            {
                zf.Add(new MemoryStream(buffer), "new", CompressionType.Deflate);
            }

            var list = new List<string>(_filenames);
            list.Add("new");
            VerifyAndDeleteTestFile(testZip, list, content);
        }

        [Fact]
        public void FileRemoveEntryTest()
        {
            var testZip = "testRemove.zip";
            var content = GenerateContent(100);
            var buffer = Encoding.ASCII.GetBytes(content);
            using (var zf = ZipFile.Open(testZip))
            {
                foreach (var filename in _filenames)
                {
                    zf.Add(new MemoryStream(buffer), filename, CompressionType.Deflate);
                }
            }

            using (var zf = ZipFile.Open(testZip))
            {
                zf.Delete("DDD");
            }

            var list = new List<string>(_filenames);
            list.Remove("DDD");
            VerifyAndDeleteTestFile(testZip, list, content);
        }

        [Fact]
        public void FileNewEntryContentTest()
        {
            var testZip = "testRemove.zip";
            var content = GenerateContent(100);
            var buffer = Encoding.ASCII.GetBytes(content);
            using (var zf = ZipFile.Open(testZip))
            {
                foreach (var filename in _filenames)
                {
                    zf.Add(new MemoryStream(buffer), filename, CompressionType.Deflate);
                }
            }

            content = GenerateContent(200);
            buffer = Encoding.ASCII.GetBytes(content);
            using (var zf = ZipFile.Open(testZip))
            {
                foreach (var filename in _filenames)
                {
                    zf.Add(new MemoryStream(buffer), filename, CompressionType.Deflate);
                }
            }

            var list = new List<string>(_filenames);
            VerifyAndDeleteTestFile(testZip, list, content);
        }
    }
}