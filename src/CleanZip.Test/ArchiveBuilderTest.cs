﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using CleanZip.Compression;
using Xunit;

namespace CleanZip.Test
{
    public class ArchiveBuilderTest
    {
        public static string ExcludeTxtFullPath = Path.Combine("..", "..", "..", "CleanZip.CmdConsole", ExcludeTxtResourceName);
        public const string ExcludeTxtResourceName = "exclude.txt";

        private int _filesAdded;
        private List<string> _files;
        private const string FileContent = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";

        [Fact]
        public void TestMock1()
        {
            var compressor = new Mock1.Target();
            var source = new Mock1.Source();
            var filter = new Mock1.Filter();
            var ab = new ArchiveBuilder(compressor);
            ab.FileAdded += ab_FileAdded;
            ab.BuildArchive(source, filter);

            Assert.Equal(compressor.AddedFiles, 2);
            Assert.Equal(_filesAdded, 2);
            Assert.True(compressor.ArchiveCreated);
            Assert.True(compressor.ArchiveClosed);
        }

        [Fact]
        public void TestZipNoFilter()
        {
            var file = @"test.zip";
            var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Test");
            if (Directory.Exists(folder)) Directory.Delete(folder, true);
            CreateFiles(folder);

            file = Path.Combine(folder, "..", file);
            if (File.Exists(file)) File.Delete(file);

            var compressor = new ZipTarget(file);
            var source = new FolderSource(folder);
            var filter = new Mock1.Filter();
            var ab = new ArchiveBuilder(compressor);
            ab.FileAdded += ab_FileAdded;
            ab.BuildArchive(source, filter);

            Assert.Equal(8, _filesAdded);

            var zf = ZipFile.OpenRead(file);
            foreach (var entry in _files)
            {
                Assert.NotNull(zf.Entries.FirstOrDefault(e => e.Name == ZipEntry.NormalizeName(entry)));
            }
            zf.Close();
            Thread.Sleep(100);
            Directory.Delete(folder, true);
            File.Delete(file);
        }
        [Fact]
        public void TestZipTxtFilter()
        {
            var file = @"testtxt.zip";
            var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestTxt");
            if (Directory.Exists(folder)) Directory.Delete(folder, true);
            CreateFiles(folder);

            file = Path.Combine(folder, "..", file);
            if (File.Exists(file)) File.Delete(file);

            var compressor = new ZipTarget(file);
            var source = new FolderSource(folder);
            var filter = new Mock1.TxtFilter();
            var ab = new ArchiveBuilder(compressor);
            ab.FileAdded += ab_FileAdded;
            ab.BuildArchive(source, filter);

            Assert.Equal(4, _filesAdded);

            var zf = ZipFile.OpenRead(file);
            foreach (var entry in _files.Where(f => f.EndsWith(".txt")))
            {
                Assert.NotNull(zf.Entries.FirstOrDefault(e => e.Name == ZipEntry.NormalizeName(entry)));
            }
            zf.Close();
            Thread.Sleep(100);
            Directory.Delete(folder, true);
            File.Delete(file);
        }

        [Fact]
        public void ZipFileCleanZipFolderTest()
        {
            var excludeFilter = new ExcludeFilter(File.ReadAllText(ExcludeTxtFullPath).Replace('\r', ' ').Replace('\n', ' '), false);
            var path = Path.Combine("..", "..", "..", "..", "..");
            var folderSource = new FolderSource(path);
            var testZip = Path.Combine(path, "TestZipCleanZipFolder1.zip");
            var archiveBuilder = new ArchiveBuilder(new ZipTarget(testZip));
            int count = 0;
            archiveBuilder.FileAdded += (s, e) => { count++; };
            var timer = Stopwatch.StartNew();
            archiveBuilder.BuildArchive(folderSource, excludeFilter);
            Console.WriteLine("Elapsed time: {0} - {1} files", timer.ElapsedMilliseconds, count);
            Thread.Sleep(100);
            File.Delete(testZip);
        }
        [Fact]
        public void ZipOutputStreamCleanZipFolderTest()
        {
            var excludeFilter = new ExcludeFilter(File.ReadAllText(ExcludeTxtFullPath).Replace('\r', ' ').Replace('\n', ' '), false);
            var path = Path.Combine("..", "..", "..", "..", "..");
            var folderSource = new FolderSource(path);
            var testZip = Path.Combine(path, "TestZipCleanZipFolder2.zip");
            var archiveBuilder = new ArchiveBuilder(new OutputStreamTarget(testZip));
            int count = 0;
            archiveBuilder.FileAdded += (s, e) => { count++; };
            var timer = Stopwatch.StartNew();
            archiveBuilder.BuildArchive(folderSource, excludeFilter);
            Console.WriteLine("Elapsed time: {0} - {1} files", timer.ElapsedMilliseconds, count);
            Thread.Sleep(100);
            File.Delete(testZip);
        }
        
        private void CreateFiles(string folder)
        {
            _files = new List<string>();
            _files.Add("test1.cs");
            _files.Add("test2.txt");
            _files.Add("test3.cs");
            _files.Add("test4.txt");
            _files.Add(Path.Combine("Sub1", "sub1.txt"));
            _files.Add(Path.Combine("Sub1", "sub2.cs"));
            _files.Add(Path.Combine("Sub1", "sub3.txt"));
            _files.Add(Path.Combine("Sub1", "sub4.cs"));
            _files.ForEach(f => CreateFile(folder, f));
        }

        private void CreateFile(string folder, string fileName)
        {
            var file = Path.Combine(folder, fileName);
            Directory.CreateDirectory(Path.GetDirectoryName(file));
            using (var stream = File.CreateText(file))
            {
                stream.WriteLine(FileContent);
            }
        }

        void ab_FileAdded(object sender, FileAddedEventArgs e)
        {
            _filesAdded++;
        }
    }
}
