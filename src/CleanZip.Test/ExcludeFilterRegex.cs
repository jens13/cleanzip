﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CleanZip.Test
{
    public class ExcludeFilterRegex : IFilter
    {

        private List<KeyValuePair<string, string>> _regexCleanup;
        private Regex _regex;

        public ExcludeFilterRegex(string exludeString)
        {
            CreateRegexCleanupDictionary();
            CreateRegex(exludeString);
        }

        private void CreateRegex(string exludeString)
        {
            var patterns = exludeString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var filter = patterns.Aggregate("", (current, pattern) => current + CleanupPattern(pattern));
            _regex = new Regex(filter.TrimEnd('|'), RegexOptions.Compiled);
        }

        private string CleanupPattern(string pattern)
        {
            var cleanPattern = _regexCleanup.Aggregate(pattern, (current, pair) => current.Replace(pair.Key, pair.Value));
            cleanPattern += cleanPattern.EndsWith(@"\\") ? "" : "$";
            return cleanPattern + "|";
        }

        private void CreateRegexCleanupDictionary()
        {
            _regexCleanup = new List<KeyValuePair<string, string>>();
            _regexCleanup.Add(new KeyValuePair<string, string>(@"\", @"\\"));
            _regexCleanup.Add(new KeyValuePair<string, string>("/", @"\\"));
            _regexCleanup.Add(new KeyValuePair<string, string>(".", @"\."));
            _regexCleanup.Add(new KeyValuePair<string, string>("*", ".*"));
            _regexCleanup.Add(new KeyValuePair<string, string>("?", "."));
            _regexCleanup.Add(new KeyValuePair<string, string>("(", @"\("));
            _regexCleanup.Add(new KeyValuePair<string, string>(")", @"\)"));
            _regexCleanup.Add(new KeyValuePair<string, string>("#", @"\d*"));
        }

        public bool Inclusive(IFile file)
        {
            return !_regex.Match(file.FileName).Success;
        }
    }
}