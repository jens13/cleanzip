﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CleanZip.Compression;
using CleanZip.Test.Mock1;
using Xunit;
using File = System.IO.File;

namespace CleanZip.Test
{
    public class ZipOutputStreamTest :ZipTestBase
    {
        [Fact]
        public void CreateStreamAndSave()
        {
            var testZip = "testCreateStream.zip";
            var files = GetFileNames(10).ToArray();
            byte[] buffer;
            using (var stream = new MemoryStream())
            {
                using (var zipOutputStream = ZipOutputStream.Wrap(stream, true))
                {
                    foreach (var file in files)
                    {
                        zipOutputStream.Add(new MemoryStream(), file, CompressionType.Store);
                    }
                    zipOutputStream.Close();
                    Assert.Throws<ObjectDisposedException>(() => zipOutputStream.ToArray());
                }
                // 20.	Create a zip file from the images image1_1.jpg.. image63_12.jpg as images.
                //       Do not compress the jpg files, they must be saved in the same format as they were received from the DUT.
                buffer = stream.ToArray();
                    
            }
            File.WriteAllBytes(testZip, buffer);
            VerifyAndDeleteTestFile(testZip, files, "");
        }
        [Fact]
        public void WrapZipFileStreamFiles()
        {
            var testZip = "testWrap.zip";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            using (var zos = ZipOutputStream.Wrap(File.Create(testZip), false))
            {
                foreach (var file in files)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    zos.Add(fileStream, file, CompressionType.Deflate);
                }
            }
            VerifyAndDeleteTestFile(testZip, files, content);
        }

        [Fact]
        public void WrapZipFileDiskFiles()
        {
            var testZip = "testWrap.zip";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            CreateFiles(files, content);
            using (var zos = ZipOutputStream.Wrap(File.Create(testZip), false))
            {
                foreach (var file in files)
                {
                    zos.Add(file, CompressionType.Deflate);
                }
            }
            VerifyAndDeleteTestFile(testZip, files, content);
            DeleteFiles(files);
        }

        [Fact]
        public void WrapZipFileDiskFilesWithEntry()
        {
            var testZip = "testWrap.zip";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            CreateFiles(files, content);
            using (var zos = ZipOutputStream.Wrap(File.Create(testZip), false))
            {
                foreach (var file in files)
                {
                    zos.Add(file, file, CompressionType.Deflate);
                }
            }
            VerifyAndDeleteTestFile(testZip, files, content);
            DeleteFiles(files);
        }

        [Fact]
        public void WrapZipFileDiskFilesWithEntryPassword()
        {
            var testZip = "testWrap.zip";
            var password = "test";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            CreateFiles(files, content);
            using (var zos = ZipOutputStream.Wrap(File.Create(testZip), false))
            {
                foreach (var file in files)
                {
                    zos.Add(file, "test\\" + file, CompressionType.Deflate, password);
                }
            }
            var entries = files.Select(s => "test\\" + s).ToArray();
            VerifyAndDeleteTestFile(testZip, entries, content, password);
            DeleteFiles(files);
        }

        [Fact]
        public void CreateZipFile()
        {
            var testZip = "testCreate.zip";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            using (var zos = ZipOutputStream.Create())
            {
                foreach (var file in files)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    zos.Add(fileStream, file, CompressionType.Deflate);
                }
                File.WriteAllBytes(testZip, zos.ToArray());
            }
            VerifyAndDeleteTestFile(testZip, files, content);
        }

        [Fact]
        public void CreateEncryptedZipFile()
        {
            var testZip = "testCreate.zip";
            var password = "Test";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            using (var zos = ZipOutputStream.Create())
            {
                foreach (var file in files)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    zos.Add(fileStream, file, CompressionType.Deflate, password);
                }
                File.WriteAllBytes(testZip, zos.ToArray());
            }
            VerifyAndDeleteTestFile(testZip, files, content, password);
        }

        [Fact]
        public void CreateZipFile_StreamNotInZeroPos()
        {
            var testZip = "testCreate.zip";
            var content = RandomString(10 * OneKiloByte);
            var files = GetFileNames(10).ToArray();
            using (var zos = ZipOutputStream.Create())
            {
                foreach (var file in files)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    fileStream.Position = 10;
                    zos.Add(fileStream, file, CompressionType.Deflate);
                }
                File.WriteAllBytes(testZip, zos.ToArray());
            }
            VerifyAndDeleteTestFile(testZip, files, content);
        }

        [Fact]
        public void CreateZipFileDoubleTake()
        {
            var testZip = "testCreateeDoubleTake.zip";
            var content = RandomString(10 * OneKiloByte);
            var files1 = GetFileNames(10).ToArray();
            var files2 = files1.Select(s => "aa" + s).ToArray();
            using (var zos = ZipOutputStream.Create())
            {
                foreach (var file in files1)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    zos.Add(fileStream, file, CompressionType.Deflate);
                }
                using (var file = File.Create(testZip))
                {
                    var buffer = zos.ToArray();
                    file.Write(buffer, 0, buffer.Length);
                }
                VerifyAndDeleteTestFile(testZip, files1, content);
                Thread.Sleep(100);

                foreach (var file in files2)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    zos.Add(fileStream, file, CompressionType.Deflate);
                }
                using (var file = File.Create(testZip))
                {
                    var buffer = zos.ToArray();
                    file.Write(buffer, 0, buffer.Length);
                }
            }
            VerifyAndDeleteTestFile(testZip, files1.Concat(files2), content);
        }

        [Fact]
        public void CreateStreamExceptions()
        {
            Assert.Throws<ZipException>(() => ZipOutputStream.Wrap(null, false));
            Assert.Throws<ZipException>(() => ZipOutputStream.Wrap(new MockStream(true, false), false));
            Assert.Throws<ZipException>(() => ZipOutputStream.Wrap(new MockStream(false,true), false));
            var zip = ZipOutputStream.Create();
            Assert.Throws<FileNotFoundException>(() => zip.Add(".", CompressionType.Store));
        }

        private void CreateFiles(IEnumerable<string> files, string content)
        {
            foreach (var file in files)
            {
                using (var stream = File.Create(file))
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(content);
                    stream.Write(bytes, 0, bytes.Length);
                }
            }
        }
        private void DeleteFiles(IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                File.Delete(file);
            }
        }
    }
}
