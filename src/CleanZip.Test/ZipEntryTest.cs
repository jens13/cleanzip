﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CleanZip.Compression;
using Xunit;

namespace CleanZip.Test
{
    public class ZipEntryTest : ZipTestBase
    {
        [Fact]
        public void CompressionTypeTest()
        {
            var testZip = "testComp.zip";
            File.Delete(testZip);
            Thread.Sleep(100);
            using (var zf = ZipFile.Open(testZip))
            {
                zf.Add(new MemoryStream(new byte[0]), "ZeroBytes", CompressionType.Deflate);
                zf.Add(new MemoryStream(Encoding.ASCII.GetBytes("test")), "Test", CompressionType.Deflate);
                var test = "test";
                for (int i = 0; i < 1000; i++) test += "test";
                zf.Add(new MemoryStream(Encoding.ASCII.GetBytes(test)), "LargeTest", CompressionType.Deflate);
            }
            using (var zf = ZipFile.OpenRead(testZip))
            {
                var zipEntry = zf.Entries.ToArray()[0];
                Assert.Equal(CompressionType.Store, zipEntry.CompressionMethod);
                Assert.Equal(0, (int)zipEntry.CompressedSize);
                Assert.Equal(0, (int)zipEntry.UncompressedSize);

                for (int n = 1; n < 3; n++)
                {
                    zipEntry = zf.Entries.ToArray()[n];
                    if (zipEntry.CompressedSize == zipEntry.UncompressedSize)
                    {
                        Assert.Equal(CompressionType.Store, zipEntry.CompressionMethod);
                    }
                    else
                    {
                        Assert.Equal(CompressionType.Deflate, zipEntry.CompressionMethod);
                    }
                }
            }
            Thread.Sleep(100);
            File.Delete(testZip);
        }

        [Fact]
        public void TestDecryption()
        {
            var testZip = "testDecrypt.zip";
            var password = "test";
            File.Delete(testZip);
            Thread.Sleep(100);
            ResourceUtility.ExctractIfNotExists("7z_encrypt", testZip);
            TestDecryptFile(testZip, password);
            Thread.Sleep(100);
            File.Delete(testZip);
        }

        [Fact]
        public void TestDecryptionWrongPassword()
        {
            var testZip = "testDecrypt.zip";
            var password = "test1";
            File.Delete(testZip);
            Thread.Sleep(100);
            ResourceUtility.ExctractIfNotExists("7z_encrypt", testZip);
            Assert.Throws<WinZipAesException>(() => TestDecryptFile(testZip, password));
            Thread.Sleep(100);
            File.Delete(testZip);
        }

        private void TestDecryptFile(string testZip, string password)
        {
            using (var zf = ZipFile.OpenRead(testZip))
            {
                var zipEntries = zf.Entries.ToList();
                foreach (var zipEntry in zipEntries)
                {
                    Assert.NotNull(zipEntry.WinZipAesExtra);
                    var entryText = ReadToEnd(zipEntries[0], password);
                    Assert.Equal(entryText.Substring(entryText.Length - 5), " }\r\n}");
                }
            }
        }

        [Fact]
        public void TestEncryptDecryptSmall()
        {
            const string testZip = "smallEncDec.zip";
            const string password = "test";
            RunCompressionTest(testZip, 5, 5, password);
        }
        [Fact]
        public void TestEncryptDecryptMedium()
        {
            const string testZip = "mediumEncDec.zip";
            const string password = "test";
            RunCompressionTest(testZip, 5, 50, password);
        }
        [Fact]
        public void TestEncryptDecryptLarge()
        {
            const string testZip = "largeEncDec.zip";
            const string password = "test";
            RunCompressionTest(testZip, 5, 20 * OneKiloByte, password);
        }

        [Fact]
        public void TestCompressionSmall()
        {
            const string testZip = "smallComp.zip";
           RunCompressionTest(testZip, 5, 5);
        }
        [Fact]
        public void TestCompressionMedium()
        {
            const string testZip = "mediumComp.zip";
            RunCompressionTest(testZip, 5, 50);
        }

        [Fact]
        public void TestCompressionLarge()
        {
            const string testZip = "largeComp.zip";
            RunCompressionTest(testZip, 5, 20 * OneKiloByte);
        }


        [Fact]
        public void TestCompressionWithEncryption()
        {
            string testZip = "compareEnc.zip";
            Console.WriteLine("Compressing with encryption");
            RunCompressionTest(testZip, 10, OneMegaByte, "test");

            Console.WriteLine("");

            testZip = "compareComp.zip";
            Console.WriteLine("Compressing without encryption");
            RunCompressionTest(testZip, 10, OneMegaByte);
        }


        private void RunCompressionTest(string zipFile, int numberOfFiles, int contentSize, string password = null)
        {
            File.Delete(zipFile);
            Thread.Sleep(100);
            var content = RandomString(contentSize);
            var files = GetFileNames(numberOfFiles);
            var timer = Stopwatch.StartNew();
            using (var zf = ZipFile.Open(zipFile))
            {
                foreach (var file in files)
                {
                    var fileStream = new MemoryStream(Encoding.ASCII.GetBytes(content));
                    if (password == null)
                    {
                        zf.Add(fileStream, file, CompressionType.Deflate);
                    }
                    else
                    {
                        zf.Add(fileStream, file, CompressionType.Deflate, password);
                    }
                }
            }
            timer.Stop();
            Console.WriteLine("Elapsed time: {0}", timer.ElapsedMilliseconds);
            using (var zf = ZipFile.OpenRead(zipFile))
            {
                foreach (var file in files)
                {
                    var zipEntry = zf.Find(file);
                    var entryText = password == null ? ReadToEnd(zipEntry) : ReadToEnd(zipEntry, password);
                    Assert.Equal(content, entryText);
                }
            }
            Thread.Sleep(100);
            File.Delete(zipFile);
        }
    }
}
