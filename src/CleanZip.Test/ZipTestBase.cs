﻿//          Copyright Jens Granlund 2012.
//      Distributed under the New BSD License.
//     (See accompanying file notice.txt or at 
// http://www.opensource.org/licenses/bsd-license.php)

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CleanZip.Compression;
using Xunit;

namespace CleanZip.Test
{
    public class ZipTestBase
    {
        public const int OneKiloByte = 1024;
        public const int OneMegaByte = 1024 * 1024;
        private readonly Random _rng = new Random();
        private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        protected string ReadToEnd(ZipEntry zipEntry)
        {
            return new StreamReader(zipEntry.GetStream()).ReadToEnd();
        }

        protected string ReadToEnd(ZipEntry zipEntry, string password)
        {
            return new StreamReader(zipEntry.GetStream(password)).ReadToEnd();
        }

        protected string GenerateContent(int contentSize)
        {
            var content = string.Join("", Enumerable.Repeat("ABCDEFGH", 10));
            if (contentSize < 80) return content.Substring(0, contentSize);
            return string.Join(Environment.NewLine, Enumerable.Repeat(content, (int)Math.Ceiling(contentSize / 80.0))).Substring(0, contentSize);
        }

        protected string RandomString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = Chars[_rng.Next(Chars.Length)];
            }
            return new string(buffer);
        }

        protected IEnumerable<string> GetFileNames(int numberOfFiles)
        {
            var files = new List<string>();
            for (var n = 0; n < numberOfFiles; n++)
            {
                files.Add("test" + n + ".txt");
            }
            return files;
        }

        protected static void VerifyAndDeleteTestFile(string testZip, IEnumerable<string> files, string content, string password = null)
        {
            Thread.Sleep(100);
            using (var zip = ZipFile.OpenRead(testZip))
            {
                var enumerable = files as string[] ?? files.ToArray();
                Assert.Equal(enumerable.Count(), zip.Entries.Count());
                foreach (var file in enumerable)
                {
                    var entry = zip.Find(file);
                    if (entry == null) throw new Exception(file + " not found");
                    var stream = (password == null ? entry.GetStream() : entry.GetStream(password)) as MemoryStream;
                    if (stream == null) throw new Exception(file + " not found");
                    var text = Encoding.ASCII.GetString(stream.ToArray());
                    if (text != content) throw new Exception(file + " content does not match");
                }
            }
            Thread.Sleep(100);
            File.Delete(testZip);
        }


    }
}