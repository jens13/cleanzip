﻿using System.IO;
using CleanZip.Compression;

namespace CleanZip.Test
{
    public class OutputStreamArchive : IArchive
    {
                private readonly ZipOutputStream _zipFile;

                public OutputStreamArchive(string zipFile)
        {
            _zipFile = ZipOutputStream.Wrap(File.Create(zipFile), false);
        }

        public void Dispose()
        {
            _zipFile.Dispose();
        }

        public void Add(IFile file)
        {
            _zipFile.Add(file.FileName, file.EntryName, CompressionType.Deflate);
        }

        public void Close()
        {
            _zipFile.Close();
        }
 
    }
    public class OutputStreamTarget : ITarget
    {
        private readonly string _fileName;

        public OutputStreamTarget(string fileName)
        {
            _fileName = fileName;
        }

        public IArchive CreateArchive()
        {
            return new OutputStreamArchive(_fileName);
        }
    }

}